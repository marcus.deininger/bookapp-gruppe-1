package de.stuttgart.dhbw.bookapp.client.server.communication;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;

import de.stuttgart.dhbw.bookapp.sales.domain.model.Credentials;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Customer;
import de.stuttgart.dhbw.bookapp.sales.domain.model.ItemType;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Order;


public class SalesService {
		
	private static final String BASE_URL = Server.salesBaseUrl();
	
	private static final String SID = "sid";

	WebClient client = WebClient.builder()
			        .baseUrl(BASE_URL)
			        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
			        .build();
	
	
	
	public String register(Customer customer) {
		return client.post().uri("/register").bodyValue(customer).retrieve().toEntity(String.class)
				.onErrorReturn(ResponseEntity.ok(null)).block().getBody();
	}

	public String login(Credentials credentials) {
		return client.post().uri("/login").bodyValue(credentials).retrieve().toEntity(String.class)
				.onErrorReturn(ResponseEntity.ok(null)).block().getBody();
	}

	public boolean logout(String sessionId) {
		return client.put().uri("/logout")
				.cookies(cookies -> cookies.add(SID, sessionId))
				.retrieve().toEntity(boolean.class)		
				.onErrorReturn(ResponseEntity.ok(false)).block().getBody();
	}

	public boolean updateUser(String sessionId, Customer customer) {
		return client.put().uri("/update/user")
			.cookies(cookies -> cookies.add(SID, sessionId))
			.bodyValue(customer)
			.retrieve().toEntity(Boolean.class)		
			.onErrorReturn(ResponseEntity.ok(false)).block().getBody();
	}

	public boolean updatePassword(String sessionId, Credentials credentials) {
		return client.put().uri("/update/password")
				.cookies(cookies -> cookies.add(SID, sessionId))
				.bodyValue(credentials)
				.retrieve().toEntity(boolean.class)		
				.onErrorReturn(ResponseEntity.ok(false)).block().getBody();
	}

	public Customer getCustomer(String sessionId) {
		return client.get().uri("/customer")
				.cookies(cookies -> cookies.add(SID, sessionId))
				.retrieve().toEntity(Customer.class)		
				.onErrorReturn(ResponseEntity.ok(null)).block().getBody();
	}

	public boolean deleteAccount(String sessionId, Credentials credentials) {
		return client.method(HttpMethod.DELETE)
				.uri("/delete/account")
				.cookies(cookies -> cookies.add(SID, sessionId))
				.bodyValue(credentials)  // not defined for delete
				.retrieve().toEntity(boolean.class)		
				.onErrorReturn(ResponseEntity.ok(false)).block().getBody();
	}

	public List<Order> getOrders(String sessionId, ItemType itemType) {
		return client.get().uri(builder -> builder.path("/orders").path("/" + itemType.getId())
				.build()).cookies(cookies -> cookies.add(SID, sessionId))
				.retrieve().toEntityList(Order.class)		
				.onErrorReturn(ResponseEntity.ok(List.of())).block().getBody();
	}

	public boolean isAlive(String sessionId) {
		return client.get().uri("/alive")
				.cookies(cookies -> cookies.add(SID, sessionId))
				.retrieve().toEntity(boolean.class)		
				.onErrorReturn(ResponseEntity.ok(false)).block().getBody();
	}

	public boolean placeOrder(String sessionId, Order order) {
		return client.post().uri("/order")
				.cookies(cookies -> cookies.add(SID, sessionId))
				.bodyValue(order)
				.retrieve().toEntity(boolean.class)		
				.onErrorReturn(ResponseEntity.ok(false)).block().getBody();
	}
}
