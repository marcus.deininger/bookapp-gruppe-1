package de.stuttgart.dhbw.bookapp.client.server.communication;

import java.io.InputStream;
import java.util.Properties;

class Server{
	
	private static final String		PROPERTIES_FILE = "server.properties";
	private static final String		INVENTORY = "inventory";
	private static final String		SALES = "sales";

	static String inventoryBaseUrl() {
		return getProperty(PROPERTIES_FILE, INVENTORY);
	}
	
	static String salesBaseUrl() {
		return getProperty(PROPERTIES_FILE, SALES);
	}
	
	private static String getProperty(String filePath, String key) {

        Properties properties = new Properties();

        try (InputStream resourceAsStream = Server.class.getClassLoader().getResourceAsStream(filePath)) {
            properties.load(resourceAsStream);

//            System.out.println("Properties");
//            prop.forEach((k, v) -> System.out.println(k + " -> " + v));

        } catch (Exception e) {
            System.err.println("Unable to load properties file : " + filePath);
        }        

        return properties.getProperty(key);

    }

}
