package de.stuttgart.dhbw.bookapp.client.server.communication;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;

import de.stuttgart.dhbw.bookapp.inventory.domain.model.Author;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Book;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Genre;

public class InventoryService {
		
	private static final String BASE_URL = Server.inventoryBaseUrl();
	
	WebClient client = WebClient.builder()
			        .baseUrl(BASE_URL)
			        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
			        .build();
	
	
	public List<Author> getAuthors() {
		return client.get().uri("/authors").retrieve().toEntityList(Author.class)
				.onErrorReturn(ResponseEntity.ok(List.of())).block().getBody();
	}

	public List<Author> getAuthors(SliceSettings slice) {
		return client.get().uri(slice.uri("/authors")).retrieve().toEntityList(Author.class)
				.onErrorReturn(ResponseEntity.ok(List.of())).block().getBody();
	}

	public List<Genre> getGenres() {
		return client.get().uri("/genres").retrieve().toEntityList(Genre.class)
				.onErrorReturn(ResponseEntity.ok(List.of())).block().getBody();
	}

	public List<Genre> getGenres(SliceSettings slice) {
		return client.get().uri(slice.uri("/genres")).retrieve().toEntityList(Genre.class)
				.onErrorReturn(ResponseEntity.ok(List.of())).block().getBody();
	}

	public List<Book> getBooks() {
		return client.get().uri("/books").retrieve().toEntityList(Book.class)
				.onErrorReturn(ResponseEntity.ok(List.of())).block().getBody();
	}

	public List<Book> getBooks(SliceSettings slice) {
		return client.get().uri(slice.uri("/books")).retrieve().toEntityList(Book.class)
						.onErrorReturn(ResponseEntity.ok(List.of())).block().getBody();
	}

	public Book getBook(int bookId) {
		return client.get().uri(builder -> builder.path("/book").path("/" + bookId)
			    			.build()).retrieve().toEntity(Book.class)
							.onErrorReturn(ResponseEntity.ok(null)).block().getBody();
	}

	public Book getBook(Book book) {
		return getBook(book.getId());
	}

	public List<Book> getBooksByGenre(int genreId) {
		return client.get().uri(builder -> builder.path("/books")
											.queryParam("genreId", genreId)
							.build()).retrieve().toEntityList(Book.class)
							.onErrorReturn(ResponseEntity.ok(List.of())).block().getBody();
	}

	public List<Book> getBooksByGenre(int genreId, SliceSettings slice) {
		return client.get().uri(slice.build(builder -> builder.path("/books")
											.queryParam("genreId", genreId))
							).retrieve().toEntityList(Book.class)
							.onErrorReturn(ResponseEntity.ok(List.of())).block().getBody();
	}

	public List<Book> getBooksByGenre(Genre genre) {
		return getBooksByGenre(genre.getId());
	}

	public List<Book> getBooksByAuthor(int authorId) {
		return client.get().uri(builder -> builder.path("/books")
													.queryParam("authorId", authorId)
						.build()).retrieve().toEntityList(Book.class)
						.onErrorReturn(ResponseEntity.ok(List.of())).block().getBody();
	}

	public List<Book> getBooksByAuthor(int authorId, SliceSettings slice) {
		return client.get().uri(slice.build(builder -> builder.path("/books")
													.queryParam("authorId", authorId))
						).retrieve().toEntityList(Book.class)
						.onErrorReturn(ResponseEntity.ok(List.of())).block().getBody();
	}

	public List<Book> getBooksByAuthor(Author author) {
		return getBooksByAuthor(author.getId());
	}

	public List<Book> getBooksByAuthorAndGenre(Author author, Genre genre) {
		return client.get().uri(builder -> builder.path("/books")
													.queryParam("authorId", author.getId())
			    									.queryParam("genreId", genre.getId())
					.build()).retrieve().toEntityList(Book.class)
					.onErrorReturn(ResponseEntity.ok(List.of())).block().getBody();

	}
	
	public int getNumberOfAuthors() {
		return client.get().uri("/count/authors").retrieve().toEntity(int.class)
				.onErrorReturn(ResponseEntity.ok(0)).block().getBody();
	}

	public int getNumberOfGenres() {
		return client.get().uri("/count/genres").retrieve().toEntity(int.class)
				.onErrorReturn(ResponseEntity.ok(0)).block().getBody();
	}

	public int getNumberOfBooks() {
		return client.get().uri("/count/books").retrieve().toEntity(int.class)
				.onErrorReturn(ResponseEntity.ok(0)).block().getBody();
	}

	public int getNumberOfBooksByGenre(int genreId) {
		return client.get().uri(builder -> builder.path("/count/books")
			    									.queryParam("genreId", genreId)
					.build()).retrieve().toEntity(int.class)
					.onErrorReturn(ResponseEntity.ok(0)).block().getBody();
	}
	
	public int getNumberOfBooksByAuthor(int authorId) {
		return client.get().uri(builder -> builder.path("/count/books")
									.queryParam("authorId", authorId)
					.build()).retrieve().toEntity(int.class)
					.onErrorReturn(ResponseEntity.ok(0)).block().getBody();
	}
	
	public int getNumberOfBooksByAuthorAndGenre(int authorId, int genreId) {
		return client.get().uri(builder -> builder.path("/count/books")
													.queryParam("authorId", authorId)
			    									.queryParam("genreId", genreId)
					.build()).retrieve().toEntity(int.class)
					.onErrorReturn(ResponseEntity.ok(0)).block().getBody();
	}

	public static void main(String[] args) {
			WebClient client = WebClient.builder()
			        .baseUrl(BASE_URL)
	//		        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
			        .build();
			
	//		ResponseEntity<String> response = client.get().uri("/").retrieve().toEntity(String.class).block();
	//		String body = response.getBody();
	//		System.out.println(body);
			
	//		ResponseEntity<List<Author>> response = client.get().uri("/authors").retrieve().toEntityList(Author.class).block();
	//		List<Author> body = response.getBody();
	//		System.out.println(body);
	
	//		ResponseEntity<Book> response = client.get().uri("/books/1").retrieve().toEntity(Book.class).block();
	//		Book body = response.getBody();
	//		System.out.println(body);
	
	//		ResponseEntity<List<Book>> response = client.get().uri("/books/author/1").retrieve().toEntityList(Book.class).block();
	//		List<Book> body = response.getBody();
	//		System.out.println(body);
	
	//		ResponseEntity<List<Book>> response = client.get().uri("/books/genre/1").retrieve().toEntityList(Book.class).block();
	//		List<Book> body = response.getBody();
	//		System.out.println(body);
	
			ResponseEntity<List<Book>> response = client.get().uri("/books?authorId=1&?genreId=1").retrieve().toEntityList(Book.class).onErrorReturn(ResponseEntity.ok(List.of())).block();
			List<Book> body = response.getBody();
			System.out.println(body);
		}
}
