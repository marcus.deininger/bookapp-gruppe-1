package de.stuttgart.dhbw.bookapp.client.server.communication;

import static de.stuttgart.dhbw.bookapp.client.server.communication.SliceSettings.Direction.*;
import static de.stuttgart.dhbw.bookapp.client.server.communication.SliceSettings.Field.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.springframework.web.util.UriBuilder;


public class SliceSettings {
		
	static enum Field {
		
		TITLE("title"), AUTHOR("author"), GENRE("genre"), YEAR("year"), UNSET("unset");
		
		String name;

		private Field(String name) {
			this.name = name;
		}
	}
	
	static enum Direction {
		UNORDERED("unordered"), ASCENDING("ascending"), DESCENDING("descending");
		
		String name;

		private Direction(String name) {
			this.name = name;
		}
		
		Direction next(){
			switch(this){
			case UNORDERED: return ASCENDING;
			case ASCENDING: return DESCENDING;
			case DESCENDING: return ASCENDING;
			}
			return null;
		}
	}
	
	public class Page {
		
		private int number, from, to;
		private boolean delimiter;
		
		public Page() {
			delimiter = true;
		}
		
		public Page(int number) {
			this.number = number;
			delimiter = false;
			from = (number - 1) * INTERVALL + 1;
			to = from + INTERVALL - 1;
			if(to > SliceSettings.this.elementCount)
				to = SliceSettings.this.elementCount;
		}
		
		public boolean shouldLink() {
			return !delimiter && number != SliceSettings.this.currentPage;
		}

		public int getNumber() {
			return number;
		}

		public int getFrom() {
			return from;
		}

		public int getTo() {
			return to;
		}
		
		public String getDelimiter() {
			int n = elementCount / INTERVALL + (elementCount / INTERVALL == 0 ? 0 : 1);
			if(number < n)
				return " | ";
			else
				return "";
		}
		
		public String toString() {
			if(delimiter)
				return "...";
			else
				return from + " - " + to;
		}
	}
	
	private static int INTERVALL = 20;
	private static int BORDER = 2;
	
	private int currentPage = 1;
	private Optional<Integer> from = Optional.of(1);
	private Optional<Integer> to = Optional.of(INTERVALL);
	private Field field = TITLE;
	private Direction direction = ASCENDING;
	
	private int elementCount = 0;
	private int pageCount = 0;
	
	public static SliceSettings of(int from, int to) {
		SliceSettings settings = new SliceSettings();
		settings.from = Optional.of(from);
		settings.to = Optional.of(to);
		return settings;
	}
	
	public int getPageCount() {
		return pageCount;
	}

	private Field decode(Optional<String> optFieldParam) {
		if(optFieldParam.isEmpty())
			return null;
		
		String fieldParam = optFieldParam.get();
		
		for(Field field : Field.values())
			if(fieldParam.equalsIgnoreCase(field.name))
				return field;
		
		return UNSET;
	}
	
	public SliceSettings setCount(int count) {
		elementCount = count;
		pageCount = elementCount / INTERVALL + (elementCount / INTERVALL == 0 ? 0 : 1);
		return this;
	}
	
	public List<Page> getPages(){
		List<Page> pages = new ArrayList<>();
		
		int i;
		for(i = 1; i <= (BORDER + 1) && i <= pageCount; i++)
			pages.add(new Page(i));
		
		if(i < currentPage - (BORDER + 1)) {
			pages.add(new Page());
			i = currentPage - BORDER;
		} else
			i = Math.max(i, currentPage - BORDER);
		
		for(; i <= currentPage + BORDER && i <= pageCount; i++)
			pages.add(new Page(i));

		
		if(currentPage + BORDER + 1 < pageCount) {
			pages.add(new Page());
			i = pageCount - BORDER;
		} else
			i = Math.max(i, Math.max(currentPage + BORDER + 1, pageCount - BORDER));		
		
		for(; i <= pageCount; i++)
			pages.add(new Page(i));
				
		return pages;
	}
	
	public SliceSettings setField(Optional<String> optFieldParam) {
		if(optFieldParam.isEmpty())
			return this;
		Field newField = decode(optFieldParam);
		if(newField == UNSET) {
			field = UNSET;
			direction = UNORDERED;
		} else if(newField == field) {
			direction = direction.next();
		} else {
			field = newField;
			direction = ASCENDING;
		}
		return this;
	}
	
	public SliceSettings setPage(Optional<Integer> page) {
		if(page.isEmpty())
			return this;
		
		this.currentPage = page.get();
		int from = ((this.currentPage - 1) * INTERVALL) + 1;
		int to = from + INTERVALL - 1;
		
		this.from = Optional.of(from);
		this.to = Optional.of(to);

		return this;
	}

	public Function<UriBuilder, URI> uri(String path) {
		Function<UriBuilder, UriBuilder> pathUriBuilder = builder -> builder.path(path);
		Function<UriBuilder, URI> uriSupplier = this.build(pathUriBuilder);		
		return uriSupplier;
	}

	
	public Function<UriBuilder, URI> build(Function<UriBuilder, UriBuilder> pathUriBuilder) {
	
		Function<UriBuilder, UriBuilder> fromUriBuilder = (from.isPresent() ? 
			builder -> pathUriBuilder.apply(builder).queryParam("from", from.get()) : pathUriBuilder);
		Function<UriBuilder, UriBuilder> toUriBuilder = (to.isPresent() ? 
				builder -> fromUriBuilder.apply(builder).queryParam("to", to.get()) : fromUriBuilder);
		Function<UriBuilder, UriBuilder> typeUriBuilder = (field != UNSET ? 
				builder -> toUriBuilder.apply(builder).queryParam("sort-field", field.name) : toUriBuilder);
		Function<UriBuilder, UriBuilder> directionUriBuilder = (direction != UNORDERED ? 
				builder -> typeUriBuilder.apply(builder).queryParam("sort-direction", direction.name) : typeUriBuilder);
				
		Function<UriBuilder, URI> uriSupplier = builder -> directionUriBuilder.apply(builder).build();
		
		return uriSupplier;
	}
}
