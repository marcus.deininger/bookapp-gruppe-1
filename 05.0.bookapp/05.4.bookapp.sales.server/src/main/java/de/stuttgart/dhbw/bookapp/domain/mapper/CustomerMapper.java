package de.stuttgart.dhbw.bookapp.domain.mapper;

import de.stuttgart.dhbw.bookapp.db.entities.CustomerEntity;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Customer;

public class CustomerMapper extends Mapper<Customer, CustomerEntity> {

	@Override
	public Customer toDomain(CustomerEntity entity) {
		return new Customer(entity.getId(), entity.getFirstname(), entity.getLastname(), 
				entity.getStreet(), entity.getCity(), entity.getEmail(), 
				entity.getUsername(), entity.getPassword());
	}

	@Override
	public CustomerEntity toEntity(Customer customer) {
		return new CustomerEntity(customer.getId(), customer.getFirstname(), customer.getLastname(), 
				customer.getStreet(), customer.getCity(), customer.getEmail(), 
				customer.getUsername(), customer.getPassword());
	}
}
