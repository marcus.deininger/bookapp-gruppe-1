package de.stuttgart.dhbw.bookapp.server.main;

// see: https://stackoverflow.com/questions/31134333/this-application-has-no-explicit-mapping-for-error
// Make sure that your main class is in a root package above other classes.
// When you run a Spring Boot Application, (i.e. a class annotated with @SpringBootApplication), 
// Spring will only scan the classes below your main class package.

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("de.stuttgart.dhbw.bookapp.server")
public class StartSalesServer {

	public static void main(String[] args) {
		SpringApplication.run(StartSalesServer.class, args);
	}

}
