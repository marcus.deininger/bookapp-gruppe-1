package de.stuttgart.dhbw.bookapp.db.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "CUSTOMERS")
public class CustomerEntity {
		
	public static final String ID = "ID";
	public static final String FIRSTNAME = "FIRSTNAME";
	public static final String LASTNAME = "LASTNAME";
	public static final String STREET = "STREET";
	public static final String CITY = "CITY";
	public static final String EMAIL = "EMAIL";
	public static final String USERNAME = "USERNAME";
	public static final String PASSWORD = "PASSWORD";
	
	@DatabaseField(columnName = ID, generatedId = true) private int id;
	@DatabaseField(columnName = FIRSTNAME) private String firstname;
	@DatabaseField(columnName = LASTNAME) private String lastname;
	@DatabaseField(columnName = STREET) private String street;
	@DatabaseField(columnName = CITY) private String city;
	@DatabaseField(columnName = EMAIL) private String email;
	@DatabaseField(columnName = USERNAME) private String username;
	@DatabaseField(columnName = PASSWORD) private String password;

	public CustomerEntity() {}
	
	// needed for mapper
	public CustomerEntity(int id, String firstname, String lastname, String street, String city, String email, String username, String password) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.street = street;
		this.city = city;
		this.email = email;
		this.username = username;
		this.password = password;
	}



	public int getId() {
		return id;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getStreet() {
		return street;
	}

	public String getCity() {
		return city;
	}

	public String getEmail() {
		return email;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public String toString() {
		return "CustomerEntity [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", street=" + street
				+ ", city=" + city + ", email=" + email + ", username=" + username + ", password=" + password + "]";
	}
}
