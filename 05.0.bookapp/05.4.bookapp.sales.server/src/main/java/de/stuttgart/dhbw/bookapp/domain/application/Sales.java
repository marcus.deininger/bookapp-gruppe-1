package de.stuttgart.dhbw.bookapp.domain.application;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import de.stuttgart.dhbw.bookapp.db.access.SalesDBAccess;
import de.stuttgart.dhbw.bookapp.domain.filter.CustomerFilter;
import de.stuttgart.dhbw.bookapp.domain.util.SendinBlueMailServer;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Credentials;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Customer;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Order;


public class Sales {
		
	private static SalesDBAccess salesDB = new SalesDBAccess();
	private static SendinBlueMailServer mailer = new SendinBlueMailServer();


	private static final int TIMEOUT = 60; // Minutes

	private static final SecureRandom SECURE_RANDOM = new SecureRandom(); //threadsafe
	private static final Base64.Encoder BASE_64_ENCODER = Base64.getUrlEncoder(); //threadsafe
	
	private static final CustomerFilter CF = new CustomerFilter();

	private static class Session {
		
		Session(Customer customer, Thread thread) {
			this.customer = customer;
			this.thread = thread;
		}
		
		Customer customer;
		Thread thread;
	}

	private Map<String, Session> sessions = new HashMap<>();
	
	private String newSession(Customer customer) {
		// see https://stackoverflow.com/questions/13992972/how-to-create-a-authentication-token-using-java
		byte[] randomBytes = new byte[24];
	    SECURE_RANDOM.nextBytes(randomBytes);
	    String sessionId = BASE_64_ENCODER.encodeToString(randomBytes);
	    
		Thread thread = new Thread(() -> {
			try {
				Thread.sleep(TIMEOUT * 60 * 1000); // will be interrupted on logout
			} catch (InterruptedException e) {}
			sessions.remove(sessionId);
		});
		sessions.put(sessionId, new Session(customer, thread));		
		thread.start();
	    	    
	    return sessionId;
	}	
	
	private byte[] toBytes(char[] chars) {
		// see: https://stackoverflow.com/questions/5513144/converting-char-to-byte
		CharBuffer charBuffer = CharBuffer.wrap(chars);
		ByteBuffer byteBuffer = Charset.forName("UTF-8").encode(charBuffer);
		byte[] bytes = Arrays.copyOfRange(byteBuffer.array(),
				byteBuffer.position(), byteBuffer.limit());
		Arrays.fill(byteBuffer.array(), (byte) 0); // clear sensitive data
		return bytes;
	}
	
	private byte[] generateSalt() {
		// see: https://howtodoinjava.com/java/java-security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
        try {
			SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
			byte[] salt = new byte[16];
			sr.nextBytes(salt);
			return salt;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
        return null;
    }
	
	private String hashPassword(char[] passwordToHash, byte[] salt) {
        String hashedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt);
            byte[] bytes = md.digest(toBytes(passwordToHash));
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            hashedPassword = sb.toString();
        } 
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return hashedPassword;
    }
	
	private String hashPasswordWithSalt(char[] passwordToHash) {
		byte[] salt = generateSalt();
		String hashedPw = hashPassword(passwordToHash, salt);
		String saltStr = Base64.getEncoder().encodeToString(salt);		
		String hashedPwSalt = hashedPw + ";" + saltStr;				
		return hashedPwSalt;
	}
	
	private boolean verifyPassword(char[] passwordToValidate, String encodedPassword) {
		if(passwordToValidate == null || encodedPassword == null)
			return false;
		
		int pos = encodedPassword.indexOf(";");
		String hashedPassword = encodedPassword.substring(0, pos);
		String encodedSalt = encodedPassword.substring(pos + 1);
		
		byte[] salt = Base64.getDecoder().decode(encodedSalt);
		String hashedPasswordToValidate = hashPassword(passwordToValidate, salt);
		
		return hashedPassword.equals(hashedPasswordToValidate);
	}
	
	private boolean valid(String str) {
		return str != null && !str.isBlank();
	}
	
	private boolean valid(char[] str) {
		if(str == null || str.length == 0)
			return false;
		for(char c : str)
			if(!Character.isWhitespace(c))
				return true;
		return false;
	}

	private boolean validEmail(String email) {
		if(!valid(email))
			return false;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
		   	return false;
		}
		return true;
	}
	
	private boolean validUsername(String username) {
		if(!valid(username))
			return false;
		if(!Pattern.matches("[a-zA-Z0-9\\.]+", username))
			return false;
		return true;
	}
	
	private boolean validPassword(char[] password) {
		if(!valid(password))
			return false;
		if(password.length < 6)
			return false;
		return true;
	}
	
	String register(Customer customer) {
		char[] password = customer.getPlainPassword();
		
		 if(!validPassword(password))
				return null; // "Wrong field format."
		
		if(!valid(customer.getFirstname()) || !valid(customer.getLastname())
				|| !valid(customer.getStreet()) || !valid(customer.getCity())
				|| !validEmail(customer.getEmail()) 
				|| !validUsername(customer.getUsername()))
			return null; // "Wrong field format."
		
		if(validUsername(customer.getUsername()) && salesDB.getCustomerByUsername(customer.getUsername()) != null)
			return null; // "Username already taken"

		String hashedPassword = hashPasswordWithSalt(password);				
		customer = CF.toServer(customer, hashedPassword);
		
		customer = salesDB.storeCustomer(customer);
		if(customer == null)
			return null;
			
		// finally succeeded		
		String sessionId = newSession(customer);	
		return sessionId;
	}
	
	boolean updateUser(String sessionId, Customer customer) {
		
		if(sessionId == null)
			return false; // "Invalid token"
		
		Session session = sessions.get(sessionId);
		if(session == null)
			return false; // "Invalid session"
				
		if(!valid(customer.getFirstname()) || !valid(customer.getLastname())
				|| !valid(customer.getStreet()) || !valid(customer.getCity())
				|| !validEmail(customer.getEmail()))
			return false; // "Wrong field format."
		

		Customer updating = new Customer(session.customer.getId(), customer.getFirstname(), customer.getLastname(),
										customer.getStreet(), customer.getCity(), customer.getEmail(), 
										session.customer.getUsername(), session.customer.getPassword());
		
		updating = salesDB.updateCustomer(updating);
		if(updating == null)
			return false; // "Update failed"
		
		session.customer = updating;
		return true;
	}

	boolean updatePassword(String sessionId, Credentials credentials) {

		char[] currentPassword = credentials.getPassword();
		char[] newPassword = credentials.getNewPassword();
		
		if(sessionId == null)
			return false; // "Invalid token"
		
		Session session = sessions.get(sessionId);
		if(session == null)
			return false; // "Invalid session"
				
		if(!validPassword(newPassword))
			return false; // "Wrong field format."
		
		if(!verifyPassword(currentPassword, session.customer.getPassword())) 
			return false; // "Wrong password"
				
		//verified
		String hashedPassword = hashPasswordWithSalt(newPassword);				
		Customer updating = session.customer.with(hashedPassword);		
		updating = salesDB.updateCustomer(updating);
		if(updating == null)
			return false; // "Update failed"
		
		session.customer = updating;
		return true;
	}

	boolean deleteAccount(String sessionId, Credentials credentials) {

		char[] password = credentials.getPassword();
		
		if(sessionId == null)
			return false; // "Invalid token"
		
		Session session = sessions.get(sessionId);
		if(session == null)
			return false; // "Invalid session"
				
		if(!validPassword(password))
			return false; // "Wrong field format."
		
		//verified
		int deleted = salesDB.deleteCustomer(session.customer);
		if(deleted == 0)
			return false; // "Deletion failed"
		
		logout(sessionId);
		return true;
	}

	String login(Credentials credentials) {
		
		String username = credentials.getUsername();
		char[] password = credentials.getPassword();

		// Data Validation
		if(!validUsername(username) || !validPassword(password))
			return null;
				
		Customer customer = salesDB.getCustomerByUsername(username);
		if(customer == null)
			return null;
		
		if(!verifyPassword(password, customer.getPassword())) 
			return null;
				
		//verified
		for(String sessionId : sessions.keySet())
			if(sessions.get(sessionId).customer.getUsername().equalsIgnoreCase(customer.getUsername()))
				return sessionId;
		
		String sessionId = newSession(customer);
		return sessionId;
	}
	
	boolean logout(String sessionId) {
		if(sessionId == null)
			return false; // "Invalid token"
		Session session = sessions.get(sessionId);
		if(session == null)
			return false; // "Invalid session"
		
		session.thread.interrupt();
		return true;
	}
	
	Customer getCustomer(String sessionId) {
		if(sessionId == null)
			return null; // "Invalid token"
		Session session = sessions.get(sessionId);
		if(session == null)
			return null; // "Invalid session"

		Customer customer = session.customer;
		return customer;
	}

	List<Order> getOrders(String sessionId, int itemTypeId) {
		if(sessionId == null)
			return new ArrayList<>();	
		
		Session session = sessions.get(sessionId);
		if(session == null)
			return new ArrayList<>();	
			
		return salesDB.getOrdersByCustomerAndType(session.customer.getId(), itemTypeId);
	}

	boolean isAlive(String sessionId) {
		if(sessionId == null)
			return false;
		
		return sessions.get(sessionId) != null;
	}
	
	boolean placeOrder(String sessionId, Order order) {
		
		if(sessionId == null)
			return false;
		
		Customer customer = getCustomer(sessionId);
		if(customer == null)
			return false;
		
			
		order = new Order(0, customer, order.getItemType(), order.getItemId(), 
				LocalDateTime.now(), order.getHeader(), order.getBody());
		salesDB.storeOrder(order);
		
		mailer.sendMail(customer.getEmail(), customer.getFirstname() + " " + customer.getLastname(), 
				"Your order of '" + order.getHeader() + "'", messageBody(order));
		
		return true;
	}
	
	@SuppressWarnings("static-access")
	private String messageBody(Order order) {
		Customer customer = order.getCustomer();
		
		return "<html><body>Dear " + customer.getFirstname() + ",<p>"
					+ "this is a confirmation of your order of " + order.getBody() +".<p>" 
					+ "It will be sent to: " + customer.getStreet() + ", " + customer.getCity() + ".<p>"
					+ "Thanks for ordering at our shop.<p>"
					+ mailer.getSenderName() + "</body></html>";
	}

}
