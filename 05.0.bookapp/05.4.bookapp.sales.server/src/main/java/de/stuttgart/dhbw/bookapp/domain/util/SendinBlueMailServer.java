package de.stuttgart.dhbw.bookapp.domain.util;

import java.util.ArrayList;
import java.util.List;

// Java SDK: https://github.com/sendinblue/APIv3-java-library
import sendinblue.ApiClient;
import sendinblue.ApiException;
import sendinblue.Configuration;
import sendinblue.auth.ApiKeyAuth;
import sibApi.TransactionalEmailsApi;
import sibModel.CreateSmtpEmail;
import sibModel.SendSmtpEmail;
import sibModel.SendSmtpEmailSender;
import sibModel.SendSmtpEmailTo;

public class SendinBlueMailServer {

	private static final String API_KEY = Mail.apiKey();

	private static final String SENDER_NAME = Mail.senderName();
	private static final String SENDER_MAIL = Mail.senderMail();
	
	public static String getSenderName() {
		return SENDER_NAME;
	}

	public static String getSenderMail() {
		return SENDER_MAIL;
	}

	public CreateSmtpEmail sendMail(String fromMail, String fromName, String toMail, 
							String toName, String subject, String body, boolean printOnFail) {

		CreateSmtpEmail response = null;
		ApiClient defaultClient = Configuration.getDefaultApiClient();
		ApiKeyAuth apiKey = (ApiKeyAuth) defaultClient.getAuthentication("api-key");
		apiKey.setApiKey(API_KEY);

		try {
			TransactionalEmailsApi api = new TransactionalEmailsApi();
			SendSmtpEmailSender sender = new SendSmtpEmailSender();
			sender.setEmail(fromMail);
			sender.setName(fromName);
			
			List<SendSmtpEmailTo> toList = new ArrayList<SendSmtpEmailTo>();
			SendSmtpEmailTo to = new SendSmtpEmailTo();
			to.setEmail(toMail);
			to.setName(toName);
			toList.add(to);

			SendSmtpEmail sendSmtpEmail = new SendSmtpEmail();
			sendSmtpEmail.setSender(sender);
			sendSmtpEmail.setTo(toList);

			sendSmtpEmail.setSubject(subject);
			sendSmtpEmail.setHtmlContent(body);
			response = api.sendTransacEmail(sendSmtpEmail);
//			System.out.println(response);
			System.out.println("Sucessfully sent mail");
		} catch (ApiException e) {
			System.err.println(e.getResponseBody());
			if(!printOnFail)
				e.printStackTrace();
			else
				System.out.println("=========================================\n"
						+ "To: " + toName + "<" + toMail +"\n"
						+ "Subject: " + subject + "\n"
						+ body + "\n=========================================");
		}
		return response;
	}
	
	public CreateSmtpEmail sendMail(String toMail, String toName, String subject, String body) {
		return sendMail(SENDER_MAIL, SENDER_NAME, toMail, toName, subject, body, true);
	}
 
}
