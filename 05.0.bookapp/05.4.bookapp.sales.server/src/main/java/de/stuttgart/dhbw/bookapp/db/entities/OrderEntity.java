package de.stuttgart.dhbw.bookapp.db.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ORDERS")
public class OrderEntity {

	public static final String ID = "ID";
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	public static final String ITEM_TYPE= "ITEM_TYPE";
	public static final String ITEM_ID = "ITEM_ID";
	public static final String ORDER_DATE = "ORDER_DATE";
	
	@DatabaseField(columnName = ID, generatedId = true) private int id;
	@DatabaseField(columnName = CUSTOMER_ID, canBeNull = false, foreign = true) private CustomerEntity customer;
	@DatabaseField(columnName = ITEM_TYPE, canBeNull = false) private int itemType;
	@DatabaseField(columnName = ITEM_ID, canBeNull = false) private int itemId;
	@DatabaseField(columnName = ORDER_DATE) private long orderDate;

	public OrderEntity() {}

	public OrderEntity(int id, CustomerEntity customer, int itemType, int itemId, long orderDate) {
		this.id = id;
		this.customer = customer;
		this.itemType = itemType;
		this.itemId = itemId;
		this.orderDate = orderDate;
	}


	public int getId() {
		return id;
	}

	public CustomerEntity getCustomer() {
		return customer;
	}

	public int getItemType() {
		return itemType;
	}

	public int getItemId() {
		return itemId;
	}

	public long getOrderDate() {
		return orderDate;
	}

	@Override
	public String toString() {
		return "OrderEntity [id=" + id + ", customer=" + customer + ", itemType=" + itemType + ", itemId=" + itemId
				+ ", orderDate=" + orderDate + "]";
	}
}
