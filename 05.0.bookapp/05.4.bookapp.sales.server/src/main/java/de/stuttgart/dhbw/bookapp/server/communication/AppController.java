package de.stuttgart.dhbw.bookapp.server.communication;

import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import de.stuttgart.dhbw.bookapp.domain.application.Application;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Credentials;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Customer;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Order;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(
	    info = @Info(
	        title = "Sales-App",
	        description = "A Web Service to organize sales.",
	        version = "3.0"
	    )
	)

// Call Swagger with: http://localhost:8080/api/v3/swagger-ui.html
// Call App with: http://localhost:8080/api/v3/...

@RestController
public class AppController {

	private static final String SID = "sid";

	private static Application app = new Application();

	@Operation(summary = "Welcome message", description = "Returns a welcome message.")
	@GetMapping(path="", produces = "text/plain")
	public String welcome() {
 	   //	Call with: http://localhost:8080/api/v3
		return "Welcome to the Sales-App!";
	}

	@Operation(summary = "Login as a customer", description = "Login as a customer with the given userid and password.")
	@PostMapping(path = "/login", consumes = "application/json", produces = "application/json")
	public String login(@RequestBody Credentials credentials, HttpServletResponse response) {
    	String sessionId = app.login(credentials);
    	if(sessionId != null)
    		response.addCookie(new Cookie(SID, sessionId));
    	return sessionId;
    }
	
	@Operation(summary = "Logout from current session")
	@PutMapping(path = "/logout", produces = "application/json")
	public boolean logout(@CookieValue(name = SID) String sessionId) {		
		return app.logout(sessionId);		
	}
	
	@Operation(summary = "Check if a user is still logged in.")
	@GetMapping(path = "/alive", produces = "application/json")
	public boolean alive(@CookieValue(name = SID) String sessionId) {		
		return app.isAlive(sessionId);		
	}

	@Operation(summary = "Register a new customer", description = "Register a new customer with the given personal data and credentials")
	@PostMapping(path = "/register", consumes = "application/json", produces = "application/json")
	public String register(@RequestBody Customer customer, HttpServletResponse response) {
		String sessionId = app.register(customer);
		if(sessionId != null)
			response.addCookie(new Cookie(SID, sessionId));
		return sessionId;
	}

	@Operation(summary = "Update the current user", description = "Updates the current user with new user data.")
	@PutMapping(path = "/update/user", consumes = "application/json", produces = "application/json")
	public boolean updateUser(@CookieValue(name = SID) String sessionId, @RequestBody Customer customer) {		
		return app.updateUser(sessionId, customer);		
	}
	
	@Operation(summary = "Update the current password", description = "Updates the password of the currently logged in user with a new password")
	@PutMapping(path = "/update/password", consumes = "application/json", produces = "application/json")
	public boolean updatePassword(@CookieValue(name = SID) String sessionId, @RequestBody Credentials credentials) {		
		return app.updatePassword(sessionId, credentials);		
	}
	
	@Operation(summary = "Get the data of the currently logged in user")
	@GetMapping(path = "/customer", produces = "application/json")
	public Customer customer(@CookieValue(name = SID) String sessionId) {		
		return app.getCustomer(sessionId);		
	}
	
	@Operation(summary = "Delete the account of the currently logged in user", description = "Delete the account of the currently logged in user, this has to be verified with the password.")
	@DeleteMapping(path = "/delete/account", consumes = "application/json", produces = "application/json")
	public boolean deleteAccount(@CookieValue(name = SID) String sessionId, @RequestBody Credentials credentials) {		
		return app.deleteAccount(sessionId, credentials);		
	}
	
	@Operation(summary = "Sends an order", description = "Orders the given book - the customer should receive a confirmation e-mail.")
	@PostMapping(path = "/order", consumes = "application/json", produces = "application/json")
	public boolean order(@CookieValue(name = SID) String sessionId, @RequestBody Order order) {		
		return app.placeOrder(sessionId, order);		
	}

	@Operation(summary = "Return all orders of the currently logged in user.")
	@GetMapping(path = "/orders/{itemTypeId}", produces = "application/json")
	public List<Order> orders(@CookieValue(name = SID) String sessionId, @PathVariable("itemTypeId") int itemTypeId) {		
		return app.getOrders(sessionId, itemTypeId);		
	}
}
