package de.stuttgart.dhbw.bookapp.domain.application;

import java.util.List;

import de.stuttgart.dhbw.bookapp.domain.filter.CustomerFilter;
import de.stuttgart.dhbw.bookapp.domain.filter.OrderFilter;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Credentials;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Customer;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Order;

public class Application {
	
	private static Sales sales = new Sales();

	private static final OrderFilter OF = new OrderFilter();
	private static final CustomerFilter CF = new CustomerFilter();

	public boolean placeOrder(String sessionId, Order order) {
		return sales.placeOrder(sessionId, order);
	}
	
	public List<Order> getOrders(String sessionId, int itemTypeId){
		return OF.toClient(sales.getOrders(sessionId, itemTypeId));
	}
	
	public String register(Customer customer) {
		return sales.register(customer);
	}
	
	public String login(Credentials credentials) {
		return sales.login(credentials);
	}
	
	public boolean isAlive(String sessionId) {
		return sales.isAlive(sessionId);
	}
	
	public boolean logout(String sessionId) {
		return sales.logout(sessionId);
	}
	
	public Customer getCustomer(String sessionId) {
		return CF.toClient(sales.getCustomer(sessionId));
	}

	public boolean updateUser(String sessionId, Customer customer) {
		return sales.updateUser(sessionId, customer);
	}

	public boolean updatePassword(String sessionId, Credentials credentials) {
		return sales.updatePassword(sessionId, credentials);
	}

	public boolean deleteAccount(String sessionId, Credentials credentials) {
		return sales.deleteAccount(sessionId, credentials);
	}
}
