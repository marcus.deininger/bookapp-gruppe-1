package de.stuttgart.dhbw.bookapp.domain.filter;

import java.util.ArrayList;
import java.util.List;

public abstract class Filter<D> {
	
	public abstract D toClient(D domain);

	public List<D> toClient(List<D> domainElements){
		List<D> elements = new ArrayList<>();
		for(D domain : domainElements)
			elements.add(toClient(domain));
		return elements;
	}
}
