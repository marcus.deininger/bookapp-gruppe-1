package de.stuttgart.dhbw.bookapp.db.access;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import de.stuttgart.dhbw.bookapp.db.entities.CustomerEntity;
import de.stuttgart.dhbw.bookapp.db.entities.OrderEntity;
import de.stuttgart.dhbw.bookapp.domain.mapper.CustomerMapper;
import de.stuttgart.dhbw.bookapp.domain.mapper.OrderMapper;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Customer;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Order;


public class SalesDBAccess {
	
	private static final String DB_URL = DbUrl.get();

	private static final CustomerMapper CUSTOMER_MAPPER = new CustomerMapper();
	private static final OrderMapper ORDER_MAPPER = new OrderMapper();
	
	public Customer getCustomerByUsername(String username) {
		try(ConnectionSource connectionSource = new JdbcConnectionSource(DB_URL)){	        
			Dao<CustomerEntity, String> customerDao = 
					DaoManager.createDao(connectionSource, CustomerEntity.class);
	        QueryBuilder<CustomerEntity, String> customerQb = customerDao.queryBuilder();
			customerQb.where().eq(CustomerEntity.USERNAME, username);
			
			PreparedQuery<CustomerEntity> preparedQuery = customerQb.prepare();
			List<CustomerEntity> customerList = customerDao.query(preparedQuery);
			
			if(customerList.isEmpty())
				return null;

			Customer customer = CUSTOMER_MAPPER.toDomain(customerList.get(0));
			return customer;
			} catch (IOException | SQLException e) {
				e.printStackTrace();
		}
		return null;
	}

	public Customer storeCustomer(Customer customer) {
		try(ConnectionSource connectionSource = new JdbcConnectionSource(DB_URL)){
			Dao<CustomerEntity, Integer> customerDao = 
				DaoManager.createDao(connectionSource, CustomerEntity.class);
			
			CustomerEntity entity = CUSTOMER_MAPPER.toEntity(customer);
			customerDao.create(entity);
			customer = CUSTOMER_MAPPER.toDomain(entity);

			return customer;
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Customer updateCustomer(Customer customer) {
		try(ConnectionSource connectionSource = new JdbcConnectionSource(DB_URL)){
			Dao<CustomerEntity, Integer> customerDao = 
				DaoManager.createDao(connectionSource, CustomerEntity.class);
			
			CustomerEntity entity = CUSTOMER_MAPPER.toEntity(customer);
			int updated = customerDao.update(entity);
			if(updated == 0)
				return null;
			customer = CUSTOMER_MAPPER.toDomain(entity);

			return customer;
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public int deleteCustomer(Customer customer) {
		try(ConnectionSource connectionSource = new JdbcConnectionSource(DB_URL)){
			Dao<OrderEntity, Integer> orderDao = 
					DaoManager.createDao(connectionSource, OrderEntity.class);

			DeleteBuilder<OrderEntity, Integer> orderDel = orderDao.deleteBuilder();
			orderDel.where().eq(OrderEntity.CUSTOMER_ID, customer.getId());			
			int deleted = orderDel.delete();
						
			Dao<CustomerEntity, Integer> customerDao = 
				DaoManager.createDao(connectionSource, CustomerEntity.class);
			
			CustomerEntity entity = CUSTOMER_MAPPER.toEntity(customer);
			deleted = customerDao.delete(entity);
			return deleted;
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public Order storeOrder(Order order) {
		try(ConnectionSource connectionSource = new JdbcConnectionSource(DB_URL)){
			Dao<OrderEntity, Integer> orderDao = 
				DaoManager.createDao(connectionSource, OrderEntity.class);
						
			OrderEntity entity = ORDER_MAPPER.toEntity(order);
			orderDao.create(entity);
			order = ORDER_MAPPER.toDomain(entity);

			return order;
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Order> getOrdersByCustomerAndType(int customerId, int itemTypeId) {
		try(ConnectionSource connectionSource = new JdbcConnectionSource(DB_URL)){	        
	        Dao<CustomerEntity, Integer> customerDao = DaoManager.createDao(connectionSource, CustomerEntity.class);	        
	        Dao<OrderEntity, Integer> orderDao = DaoManager.createDao(connectionSource, OrderEntity.class);	        

	        QueryBuilder<CustomerEntity, Integer>customerQb = customerDao.queryBuilder();
	        QueryBuilder<OrderEntity, Integer> oderQb = orderDao.queryBuilder();
	        
	        oderQb.where().eq(OrderEntity.ITEM_TYPE, itemTypeId);
	        customerQb.where().eq(CustomerEntity.ID, customerId);
        	List<OrderEntity> entities = oderQb.join(customerQb)
        			.orderBy(OrderEntity.ORDER_DATE, true).query();
        		        	        
	        List<Order> orders = ORDER_MAPPER.toDomain(entities);
	        return orders;
			} catch (IOException | SQLException e) {
				e.printStackTrace();
		}
		return new ArrayList<Order>();
	}
}
