package de.stuttgart.dhbw.bookapp.domain.filter;

import de.stuttgart.dhbw.bookapp.sales.domain.model.Customer;

public class CustomerFilter extends Filter<Customer>{

	@Override
	public Customer toClient(Customer customer) {
		return new Customer(customer.getId(), customer.getFirstname(), customer.getLastname(), 
				customer.getStreet(), customer.getCity(), customer.getEmail(), 
				customer.getUsername(), null);
	}

	public Customer toServer(Customer customer, String hashedPassword) {
		return new Customer(customer.getId(), customer.getFirstname(), customer.getLastname(), 
				customer.getStreet(), customer.getCity(), customer.getEmail(), 
				customer.getUsername(), hashedPassword);
	}

}
