package de.stuttgart.dhbw.bookapp.domain.util;

import java.io.InputStream;
import java.util.Properties;

class Mail{
	
	private static final String		PROPERTIES_FILE = "mail.properties";
	private static final String		API_KEY = "apikey";
	private static final String		SENDER_NAME = "sendername";
	private static final String		SENDER_MAIL = "sendermail";

	static String apiKey() {
		return getProperty(PROPERTIES_FILE, API_KEY);
	}
	
	static String senderName() {
		return getProperty(PROPERTIES_FILE, SENDER_NAME);
	}
	
	static String senderMail() {
		return getProperty(PROPERTIES_FILE, SENDER_MAIL);
	}
	
	private static String getProperty(String filePath, String key) {

        Properties properties = new Properties();

        try (InputStream resourceAsStream = Mail.class.getClassLoader().getResourceAsStream(filePath)) {
            properties.load(resourceAsStream);

//            System.out.println("Properties");
//            prop.forEach((k, v) -> System.out.println(k + " -> " + v));

        } catch (Exception e) {
            System.err.println("Unable to load properties file : " + filePath);
        }        

        return properties.getProperty(key);

    }

}
