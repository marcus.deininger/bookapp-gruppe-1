package de.stuttgart.dhbw.bookapp.domain.mapper;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

import de.stuttgart.dhbw.bookapp.db.entities.OrderEntity;
import de.stuttgart.dhbw.bookapp.sales.domain.model.ItemType;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Order;

public class OrderMapper extends Mapper<Order, OrderEntity> {
	
	private static final CustomerMapper CM = new CustomerMapper();
	private static final Map<Integer, ItemType> IM = new HashMap<>();
	
	static {
		for(ItemType it : ItemType.values())
			IM.put(it.getId(), it);
	}

	@Override
	public Order toDomain(OrderEntity entity) {
		return new Order(entity.getId(), 
				CM.toDomain(entity.getCustomer()), IM.get(entity.getItemType()), entity.getItemId(),
				LocalDateTime.ofInstant(Instant.ofEpochMilli(entity.getOrderDate()), ZoneId.systemDefault()), null, null);
	}


	@Override
	public OrderEntity toEntity(Order order) {
		return new OrderEntity(order.getId(), CM.toEntity(order.getCustomer()), order.getItemType().getId(), order.getItemId(),
				order.getOrderDate().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
	}

}
