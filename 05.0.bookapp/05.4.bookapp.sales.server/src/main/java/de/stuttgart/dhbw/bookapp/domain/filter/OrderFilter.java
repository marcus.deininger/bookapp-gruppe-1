package de.stuttgart.dhbw.bookapp.domain.filter;

import de.stuttgart.dhbw.bookapp.sales.domain.model.Order;

public class OrderFilter extends Filter<Order>{
	
	private static final CustomerFilter CF = new CustomerFilter();

	@Override
	public Order toClient(Order order) {
		return new Order(order.getId(), CF.toClient(order.getCustomer()), 
				order.getItemType(), order.getItemId(), order.getOrderDate(), null, null);
	}

}
