CREATE TABLE public."AUTHORS"
(
    "ID" integer NOT NULL,
    "FIRSTNAME" character varying(30) COLLATE pg_catalog."default",
    "LASTNAME" character varying(30) COLLATE pg_catalog."default",
    CONSTRAINT "AUTHORS_pkey" PRIMARY KEY ("ID")
)

TABLESPACE pg_default;

ALTER TABLE public."AUTHORS"
    OWNER to postgres;
    
CREATE TABLE public."GENRES"
(
    "ID" integer NOT NULL,
    "GENRE_NAME" character varying(30) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "GENRES_pkey" PRIMARY KEY ("ID")
)

TABLESPACE pg_default;

ALTER TABLE public."GENRES"
    OWNER to postgres;
    
CREATE TABLE public."BOOKS"
(
    "ID" integer NOT NULL,
    "TITLE" character varying(50) COLLATE pg_catalog."default" NOT NULL,
    "PUB_YEAR" integer NOT NULL,
    "AUTHOR_ID" integer NOT NULL,
    "GENRE_ID" integer NOT NULL,
    "SAMPLE" text COLLATE pg_catalog."default",
    CONSTRAINT "BOOKS_pkey" PRIMARY KEY ("ID"),
    CONSTRAINT "BOOKS_AUTHOR_ID_fkey" FOREIGN KEY ("AUTHOR_ID")
        REFERENCES public."AUTHORS" ("ID") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT "BOOKS_GENRE_ID_fkey" FOREIGN KEY ("GENRE_ID")
        REFERENCES public."GENRES" ("ID") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public."BOOKS"
    OWNER to postgres;