package de.stuttgart.dhbw.bookapp.client.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.BoxLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;

import de.stuttgart.dhbw.bookapp.client.server.communication.InventoryService;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Author;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Book;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Genre;



@SuppressWarnings("serial")
public class ShoppingView extends JFrame {
	
	private static final int HEIGHT = 600, WIDTH = 900;
	
	private JList<Author> authorList;
	private JList<Genre> genreList;
	private JList<Book> bookList;
	private JTextArea bookInfo;
	private JButton sampleButton;

	private Set<Integer> currentGenreIds;
	
	private InventoryService inventoryService;

	private ShoppingView(InventoryService inventoryService) {
		super("Bookshop");
		this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		
		this.inventoryService = inventoryService;
									
		initializeWidgets();
		JComponent content = createWidgetLayout();		
		createWidgetInteraction();

		this.setContentPane(content);
		
		this.setLocationByPlatform(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}
	
	public static void open(InventoryService inventoryService) {
		EventQueue.invokeLater(() -> {
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
					| UnsupportedLookAndFeelException e) {
				e.printStackTrace();
			}
			new ShoppingView(inventoryService);
		});
	}

	private void initializeWidgets() {
		authorList = new JList<>(inventoryService.getAuthors().toArray(new Author[0]));
		genreList = new JList<>(inventoryService.getGenres().toArray(new Genre[0]));
		bookList = new JList<>(inventoryService.getBooks().toArray(new Book[0]));
		bookInfo = new JTextArea();
		
		sampleButton = new JButton("Sample");
		sampleButton.setEnabled(false);
		
		genreList.setCellRenderer(new GenreListRenderer());
	}
	
	// see: https://stackoverflow.com/questions/6224022/how-to-make-one-item-in-a-jlist-bold
	private class GenreListRenderer extends DefaultListCellRenderer {

	    public GenreListRenderer() {
			super();
			currentGenreIds = new HashSet<>();
			ListModel<Book> model = bookList.getModel();
			for(int i = 0; i < model.getSize(); i++) {
				Book book = model.getElementAt(i);
				currentGenreIds.add(book.getId());
			}
		}

		public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
	        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

	        Genre genre = (Genre) value;
	        if (currentGenreIds.contains(genre.getId())) { // <= put your logic here
	            setFont(getFont().deriveFont(Font.BOLD));
	            setForeground(Color.BLACK);
	        } else {
	            setFont(getFont().deriveFont(Font.ITALIC));
	            setForeground(Color.GRAY);
	        } return this;
	    }
	}
	
	private JComponent createWidgetLayout() {
		int size = 12;
		authorList.setFont(new Font("Arial", Font.BOLD, size));
		genreList.setFont(new Font("Arial", Font.BOLD, size));
		bookList.setFont(new Font("Arial", Font.BOLD, size));
		bookInfo.setFont(new Font("Arial", Font.BOLD, size));

		JPanel content = new JPanel();
		content.setLayout(new GridLayout(1, 3));
		
		JPanel lists = new JPanel(new GridLayout(2, 1));
		lists.add(new JScrollPane(authorList));
		lists.add(new JScrollPane(genreList));		
		content.add(lists);
		
		content.add(new JScrollPane(bookList));
		
		JPanel bookPanel = new JPanel(new BorderLayout());
		bookPanel.add(bookInfo, BorderLayout.CENTER);
		JPanel control = new JPanel();
		control.add(sampleButton);
		bookPanel.add(control, BorderLayout.SOUTH);
		content.add(bookPanel);
		
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(content, BorderLayout.CENTER);
		
		// create the status bar panel and shove it down the bottom of the frame
		// see: https://stackoverflow.com/questions/3035880/how-can-i-create-a-bar-in-the-bottom-of-a-java-app-like-a-status-bar
		JPanel statusPanel = new JPanel();
		statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		statusPanel.setPreferredSize(new Dimension(this.getWidth(), 20));
		statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.X_AXIS));

		panel.add(statusPanel, BorderLayout.SOUTH);
		
		return panel;
	}


	private void createWidgetInteraction() {		
		authorList.addListSelectionListener(e -> {if(!e.getValueIsAdjusting()) updateList();});		
		genreList.addListSelectionListener(e -> {if(!e.getValueIsAdjusting()) updateList();});
		bookList.addListSelectionListener(e -> {if(!e.getValueIsAdjusting()) updateInfo();});
		
		sampleButton.addActionListener(e -> displaySample());
	}

	private void updateList() {
		Author selectedAuthor = authorList.getSelectedValue();
		Genre selectedGenre = genreList.getSelectedValue();
		Book[] books = null;

//		System.out.println(selectedAuthor + " " + selectedGenre + " " + selectedBook);
		if(selectedAuthor == null && selectedGenre == null)
			books = inventoryService.getBooks().toArray(new Book[0]);
		else if(selectedAuthor != null && selectedGenre == null)
			books = inventoryService.getBooksByAuthor(selectedAuthor).toArray(new Book[0]);
		else if(selectedAuthor == null && selectedGenre != null)
			books = inventoryService.getBooksByGenre(selectedGenre).toArray(new Book[0]);
		else // (selectedAuthor != null && selectedGenre != null)
			books = inventoryService.getBooksByAuthorAndGenre(selectedAuthor, selectedGenre).toArray(new Book[0]);
		
		currentGenreIds = Arrays.asList(books).stream().map(b -> b.getGenre().getId()).collect(Collectors.toSet());

		bookList.setListData(books);
		genreList.revalidate();
		genreList.repaint();
	}


	private void updateInfo() {
		Book selectedBook = bookList.getSelectedValue();
		if(selectedBook == null) {
			bookInfo.setText("");
			sampleButton.setEnabled(false);
		}else {
			refresh(selectedBook);
			Author author = selectedBook.getAuthor();
			StringBuilder text = new StringBuilder();
			text.append("Author:\t").append(author.getFirstname()).append(" ").append(author.getLastname()).append("\n");
			text.append("Title:\t").append(selectedBook.getTitle()).append("\n");
			text.append("Genre:\t").append(selectedBook.getGenre().getGenreName()).append("\n");
			text.append("Year:\t").append(selectedBook.getYear());
			bookInfo.setText(text.toString());
			sampleButton.setEnabled(true);
		}
		
	}
	
	private void refresh(Book book) {
		if(book == null)
			return;
		
		if(book.getAuthor().getFirstname() == null 
				|| book.getGenre().getGenreName() == null 
				|| book.getSample() == null) {
			Book completeBook = inventoryService.getBook(book.getId());
			book.setAuthor(completeBook.getAuthor());
			book.setGenre(completeBook.getGenre());
			book.setSample(completeBook.getSample());
		}
		
	}
	
	private void displaySample() {
		Book book = bookList.getSelectedValue();
		if(book == null)
			return;
		refresh(book);
		
		JEditorPane textPane = new JEditorPane();
		textPane.setContentType("text/html");

		Author author = book.getAuthor();
		Genre genre = book.getGenre();

		StringBuilder text = new StringBuilder();

		text.append("<body style=\"font-family:arial\">")
			.append("<div  style=\"text-align:center\">")
			.append("<h2>").append(book.getTitle()).append("</h2>\n")
			.append("<i>by</i><p>\n")
			.append("<i>").append(author.getFirstname()).append(" ")
				.append(author.getLastname()).append(", ")
				.append(book.getYear()).append(" (")
				.append(genre.getGenreName()).append(")</i><p>\n")
			.append("</div>\n")
		.append(book.getSample().replace("\\n", "\n<p>"))
		.append("</body>");

		textPane.setText(text.toString());
		textPane.setEditable(false);
		textPane.setCaretPosition(0);
		
		JDialog dialog = new JDialog(ShoppingView.this, "Sample Content", ModalityType.MODELESS);
		dialog.setPreferredSize(new Dimension(HEIGHT, HEIGHT));
		dialog.getContentPane().add(new JScrollPane(textPane));
		dialog.setLocationByPlatform(true);
		dialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		dialog.pack();
		dialog.setVisible(true);
	}
		
}