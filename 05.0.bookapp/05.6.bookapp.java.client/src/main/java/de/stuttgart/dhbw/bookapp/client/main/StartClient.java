package de.stuttgart.dhbw.bookapp.client.main;

import de.stuttgart.dhbw.bookapp.client.server.communication.InventoryService;
import de.stuttgart.dhbw.bookapp.client.server.communication.SalesService;
import de.stuttgart.dhbw.bookapp.client.view.ShoppingViewWithMicroServices;

public class StartClient {

	public static void main(String[] args) {
		InventoryService inventoryService = new InventoryService();
//        ShoppingView.open(inventoryService);

		SalesService salesService = new SalesService();
        ShoppingViewWithMicroServices.open(inventoryService, salesService);
	}

}
