package de.stuttgart.dhbw.bookapp.client.view;

import static de.stuttgart.dhbw.bookapp.client.view.UserDialog.Entry.*;
import static de.stuttgart.dhbw.bookapp.sales.domain.model.ItemType.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.BoxLayout;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;

import de.stuttgart.dhbw.bookapp.client.server.communication.InventoryService;
import de.stuttgart.dhbw.bookapp.client.server.communication.SalesService;
import de.stuttgart.dhbw.bookapp.client.view.UserDialog.Entries;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Author;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Book;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Genre;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Credentials;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Customer;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Order;


@SuppressWarnings("serial")
public class ShoppingViewWithMicroServices extends JFrame {
	
	private static final int TIMEOUT = 10; // Minutes
	private static final int HEIGHT = 600, WIDTH = 900;
	
	private JList<Author> authorList;
	private JList<Genre> genreList;
	private JList<Book> bookList;
	private JTextArea bookInfo;
	private JButton sampleButton, orderButton;
	private JMenuItem registerItem, loginItem, logoutItem, ordersItem, updateItem, passwordItem, deleteItem, exitItem;
	private JLabel statusLabel;

	private Set<Integer> currentGenreIds;
	
	private InventoryService inventoryService;
	private SalesService salesService;
	private UserDialog dialog;
	
	private String sessionId = null;
	private Thread session = null;

	private ShoppingViewWithMicroServices(InventoryService inventoryService, SalesService salesService) {
		super("Books");
		this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		
		this.inventoryService = inventoryService;
		this.salesService = salesService;
		this.dialog = new UserDialog();
									
		initializeWidgets();
		JMenuBar menuBar = createMenu();
		JComponent content = createWidgetLayout();		
		createWidgetInteraction();

		this.setJMenuBar(menuBar);
		this.setContentPane(content);
		
		this.setLocationByPlatform(true);
//		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}
	
	public static void open(InventoryService inventoryService, SalesService salesService) {
		EventQueue.invokeLater(() -> {
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
					| UnsupportedLookAndFeelException e) {
				e.printStackTrace();
			}
			new ShoppingViewWithMicroServices(inventoryService, salesService);
		});
	}

	private void initializeWidgets() {
		authorList = new JList<>(inventoryService.getAuthors().toArray(new Author[0]));
		genreList = new JList<>(inventoryService.getGenres().toArray(new Genre[0]));
		bookList = new JList<>(inventoryService.getBooks().toArray(new Book[0]));
		bookInfo = new JTextArea();
		
		sampleButton = new JButton("Sample");
		sampleButton.setEnabled(false);
		orderButton = new JButton("Order");
		orderButton.setEnabled(false);
		
		registerItem = new JMenuItem("Register new user");
		loginItem = new JMenuItem("Login");
		logoutItem = new JMenuItem("Logout");
		logoutItem.setEnabled(false);
		ordersItem = new JMenuItem("Orders");
		ordersItem.setEnabled(false);
		updateItem = new JMenuItem("Update user data");
		updateItem.setEnabled(false);
		passwordItem = new JMenuItem("Change password");
		passwordItem.setEnabled(false);
		deleteItem = new JMenuItem("Delete account");
		deleteItem.setEnabled(false);
		exitItem = new JMenuItem("Exit");
		
		statusLabel = new JLabel("not logged in");
		statusLabel.setHorizontalAlignment(SwingConstants.LEFT);
		
		genreList.setCellRenderer(new GenreListRenderer());
	}
	
	// see: https://stackoverflow.com/questions/6224022/how-to-make-one-item-in-a-jlist-bold
	private class GenreListRenderer extends DefaultListCellRenderer {

	    public GenreListRenderer() {
			super();
			currentGenreIds = new HashSet<>();
			ListModel<Book> model = bookList.getModel();
			for(int i = 0; i < model.getSize(); i++) {
				Book book = model.getElementAt(i);
				currentGenreIds.add(book.getId());
			}
		}

		public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
	        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

	        Genre genre = (Genre) value;
	        if (currentGenreIds.contains(genre.getId())) { // <= put your logic here
	            setFont(getFont().deriveFont(Font.BOLD));
	            setForeground(Color.BLACK);
	        } else {
	            setFont(getFont().deriveFont(Font.ITALIC));
	            setForeground(Color.GRAY);
	        } return this;
	    }
	}
	
	private JMenuBar createMenu() {
		//Create the menu bar.
		JMenuBar menuBar = new JMenuBar();

		//Build the first menu.
		JMenu menu = new JMenu("User Control");
		menuBar.add(menu);

		//add a group of JMenuItems
		menu.add(registerItem);
		menu.add(loginItem);
		menu.add(logoutItem);
		menu.add(ordersItem);
		menu.add(updateItem);
		menu.add(passwordItem);
		menu.add(deleteItem);
		menu.add(exitItem);

		return menuBar;
	}

	private JComponent createWidgetLayout() {
		int size = 12;
		authorList.setFont(new Font("Arial", Font.BOLD, size));
		genreList.setFont(new Font("Arial", Font.BOLD, size));
		bookList.setFont(new Font("Arial", Font.BOLD, size));
		bookInfo.setFont(new Font("Arial", Font.BOLD, size));

		JPanel content = new JPanel();
		content.setLayout(new GridLayout(1, 3));
		
		JPanel lists = new JPanel(new GridLayout(2, 1));
		lists.add(new JScrollPane(authorList));
		lists.add(new JScrollPane(genreList));		
		content.add(lists);
		
		content.add(new JScrollPane(bookList));
		
		JPanel bookPanel = new JPanel(new BorderLayout());
		bookPanel.add(bookInfo, BorderLayout.CENTER);
		JPanel control = new JPanel();
		control.add(sampleButton);
		control.add(orderButton);
		bookPanel.add(control, BorderLayout.SOUTH);
		content.add(bookPanel);
		
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(content, BorderLayout.CENTER);
		
		// create the status bar panel and shove it down the bottom of the frame
		// see: https://stackoverflow.com/questions/3035880/how-can-i-create-a-bar-in-the-bottom-of-a-java-app-like-a-status-bar
		JPanel statusPanel = new JPanel();
		statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		statusPanel.setPreferredSize(new Dimension(this.getWidth(), 20));
		statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.X_AXIS));
		statusPanel.add(statusLabel);

		panel.add(statusPanel, BorderLayout.SOUTH);
		
		return panel;
	}


	private void createWidgetInteraction() {		
		authorList.addListSelectionListener(e -> {if(!e.getValueIsAdjusting()) updateList();});		
		genreList.addListSelectionListener(e -> {if(!e.getValueIsAdjusting()) updateList();});
		bookList.addListSelectionListener(e -> {if(!e.getValueIsAdjusting()) updateInfo();});
		
		sampleButton.addActionListener(e -> displaySample());
		orderButton.addActionListener(e -> order());
		
		registerItem.addActionListener(e -> registerUser());
		loginItem.addActionListener(e -> loginUser());
		logoutItem.addActionListener(e -> logoutUser());
		ordersItem.addActionListener(e -> displayOrders());
		updateItem.addActionListener(e -> updateUserData());
		passwordItem.addActionListener(e -> changePassword());
		deleteItem.addActionListener(e -> deleteAccount());
		
		this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
            	ShoppingViewWithMicroServices.this.logoutUser();
                ShoppingViewWithMicroServices.this.dispose();
            }
        });
		
		exitItem.addActionListener(e -> {
        	ShoppingViewWithMicroServices.this.logoutUser();
            ShoppingViewWithMicroServices.this.dispose();
		});
	}

	private void updateList() {
			Author selectedAuthor = authorList.getSelectedValue();
			Genre selectedGenre = genreList.getSelectedValue();
			Book[] books = null;
	
	//		System.out.println(selectedAuthor + " " + selectedGenre + " " + selectedBook);
			if(selectedAuthor == null && selectedGenre == null)
				books = inventoryService.getBooks().toArray(new Book[0]);
			else if(selectedAuthor != null && selectedGenre == null)
				books = inventoryService.getBooksByAuthor(selectedAuthor).toArray(new Book[0]);
			else if(selectedAuthor == null && selectedGenre != null)
				books = inventoryService.getBooksByGenre(selectedGenre).toArray(new Book[0]);
			else // (selectedAuthor != null && selectedGenre != null)
				books = inventoryService.getBooksByAuthorAndGenre(selectedAuthor, selectedGenre).toArray(new Book[0]);
			
			currentGenreIds = Arrays.asList(books).stream().map(b -> b.getGenre().getId()).collect(Collectors.toSet());
	
			bookList.setListData(books);
			genreList.revalidate();
			genreList.repaint();
		}

	private void updateInfo() {
		Book selectedBook = bookList.getSelectedValue();
		if(selectedBook == null) {
			bookInfo.setText("");
			sampleButton.setEnabled(false);
			orderButton.setEnabled(false);
		}else {
			refresh(selectedBook);
			Author author = selectedBook.getAuthor();
			StringBuilder text = new StringBuilder();
			text.append("Author:\t").append(author.getFirstname()).append(" ").append(author.getLastname()).append("\n");
			text.append("Title:\t").append(selectedBook.getTitle()).append("\n");
			text.append("Genre:\t").append(selectedBook.getGenre().getGenreName()).append("\n");
			text.append("Year:\t").append(selectedBook.getYear());
			bookInfo.setText(text.toString());
			sampleButton.setEnabled(true);
			if(hasSession()) orderButton.setEnabled(true);
		}
		
	}

	private void refresh(Book book) {
		if(book == null)
			return;
		
		if(book.getAuthor().getFirstname() == null 
				|| book.getGenre().getGenreName() == null 
				|| book.getSample() == null) {
			Book completeBook = inventoryService.getBook(book);
			book.setAuthor(completeBook.getAuthor());
			book.setGenre(completeBook.getGenre());
			book.setSample(completeBook.getSample());
		}
		
	}

	private void displaySample() {
		Book book = bookList.getSelectedValue();
		if(book == null)
			return;
		refresh(book);
		
		JEditorPane textPane = new JEditorPane();
		textPane.setContentType("text/html");
	
		Author author = book.getAuthor();
		Genre genre = book.getGenre();
	
		StringBuilder text = new StringBuilder();
	
		text.append("<body style=\"font-family:arial\">")
			.append("<div  style=\"text-align:center\">")
			.append("<h2>").append(book.getTitle()).append("</h2>\n")
			.append("<i>by</i><p>\n")
			.append("<i>").append(author.getFirstname()).append(" ")
				.append(author.getLastname()).append(", ")
				.append(book.getYear()).append(" (")
				.append(genre.getGenreName()).append(")</i><p>\n")
			.append("</div>\n")
		.append(book.getSample().replace("\\n", "\n<p>"))
		.append("</body>");
	
		textPane.setText(text.toString());
		textPane.setEditable(false);
		textPane.setCaretPosition(0);
		
		JDialog dialog = new JDialog(ShoppingViewWithMicroServices.this, "Sample Content", ModalityType.MODELESS);
		dialog.setPreferredSize(new Dimension(HEIGHT, HEIGHT));
		dialog.getContentPane().add(new JScrollPane(textPane));
		dialog.setLocationByPlatform(true);
		dialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		dialog.pack();
		dialog.setVisible(true);
	}

	private void setStatus(String text) {
		statusLabel.setText(text);
	}

	private void newLocalSession(String username, String sid) {
		if(sid == null) {
			this.setStatus("Login failed");
			return;
		}
		
		this.registerItem.setEnabled(false);
		this.loginItem.setEnabled(false);
		this.logoutItem.setEnabled(true);
		this.ordersItem.setEnabled(true);
		this.updateItem.setEnabled(true);
		this.passwordItem.setEnabled(true);
		this.deleteItem.setEnabled(true);
		
		if(bookList.getSelectedValue() != null) this.orderButton.setEnabled(true);
		
		this.sessionId = sid;
	
		session = new Thread(() -> {
			try {
				Thread.sleep(TIMEOUT * 60 * 1000); // will be interrupted on logout or close
			} catch (InterruptedException e) {}
			sessionId = null;
			session = null;
			registerItem.setEnabled(true);
			loginItem.setEnabled(true);
			logoutItem.setEnabled(false);
			ordersItem.setEnabled(false);
			updateItem.setEnabled(false);
			passwordItem.setEnabled(false);
			deleteItem.setEnabled(false);
	
			orderButton.setEnabled(false);
			setStatus("not logged in");
		});
		session.start();
	
		this.setStatus("Logged in as '" + username + "'");
	}

	private boolean hasSession() {
		return sessionId != null;
	}

	private void registerUser() {
			if(sessionId != null) // another user already logged in
				return;
			
			Entries entries = dialog.registerDialog(ShoppingViewWithMicroServices.this);
			if(entries == null)
				return;
	//		System.out.println(entries);
			
			Customer customer = Customer.newCustomer(entries.get(FIRSTNAME), entries.get(LASTNAME), 
					entries.get(STREET), entries.get(CITY), entries.get(EMAIL), 
					entries.get(USERNAME), entries.getPw(PASSWORD));
			String sessionId = salesService.register(customer);
			newLocalSession(entries.get(USERNAME), sessionId);
		}

	private void updateUserData() {
		if(!hasSession())
			return;
		
		Customer customer = salesService.getCustomer(sessionId);
		Entries entries = dialog.updateDataDialog(ShoppingViewWithMicroServices.this,
				customer.getFirstname(), customer.getLastname(), 
				customer.getStreet(), customer.getCity(), customer.getEmail());
		if(entries == null)
			return;
		
		customer = Customer.updatedCustomer(entries.get(FIRSTNAME), entries.get(LASTNAME), 
				entries.get(STREET), entries.get(CITY), entries.get(EMAIL));
				
		boolean ok = salesService.updateUser(sessionId, customer);
		if(ok)
			setStatus("Sucessfully updated");
		else
			setStatus("Update failed");		
	}

	private void changePassword() {
		if(!hasSession())
			return;
	
		Entries entries = dialog.updatePasswordDialog(ShoppingViewWithMicroServices.this);
		if(entries == null)
			return;
		
		boolean ok = salesService.updatePassword(sessionId, new Credentials(entries.getPw(PASSWORD), entries.getPw(NEW_PASSWORD)));
		if(ok)
			setStatus("Sucessfully updated");
		else
			setStatus("Update failed");
	}

	private void deleteAccount() {
		if(!hasSession())
			return;
		
		Entries entries = dialog.cofirmDeletionDialog(ShoppingViewWithMicroServices.this);
		if(entries == null)
			return;
		
		boolean ok = salesService.deleteAccount(sessionId, new Credentials(entries.getPw(PASSWORD)));
		if(ok)
			this.logoutUser();
		else
			setStatus("Deletion failed");
	}

	private void loginUser() {
		if(hasSession()) // another user already logged in
			return;
				
		Entries entries = dialog.loginDialog(ShoppingViewWithMicroServices.this);
		if(entries == null)
			return;

		String sessionId = salesService.login(new Credentials(entries.get(USERNAME), entries.getPw(PASSWORD)));
		if(sessionId == null)
			return;
		
		newLocalSession(entries.get(USERNAME), sessionId);
	}

	private void logoutUser() {
		salesService.logout(sessionId);
		if(session != null)
			session.interrupt();
	}

	private void order() {
		if(!hasSession())
			return;
		
		if(!salesService.isAlive(sessionId))
			session.interrupt();
		
		Book book = bookList.getSelectedValue();
		Order order = Order.initialOrder(BOOK, book.getId(), 
				book.getTitle(), "'" + book.getTitle() + "' by " + book.getAuthor());
		
		boolean ok = salesService.placeOrder(sessionId, order);
		if(ok)
			setStatus("Sucessfully ordered");
		else
			setStatus("Order failed");
	}

	private void displayOrders() {
		if(!hasSession())
			return;
		
		List<Order> orders = salesService.getOrders(sessionId, BOOK);
		
		JEditorPane textPane = new JEditorPane();
		textPane.setContentType("text/html");
		
		StringBuilder text = new StringBuilder();
	
		text.append("<body style=\"font-family:arial\">")
			.append("<div  style=\"text-align:center\">")
			.append("<h2>Your Orders</h2>\n")
			.append("</div>\n")
			.append(orders.isEmpty() ? "<i>no orders</i>\n" : "")
			.append("<ul>\n");
		
		for(Order order : orders) {			
			Book book = inventoryService.getBook(order.getItemId());
			text.append("<li>").append(book.getTitle()).append(": ")
				.append(order.getFormattedDate())
				.append("</li>\n");
		}
		
		text.append("</ul>\n")
			.append("</body>");
	
		textPane.setText(text.toString());
		textPane.setEditable(false);
		textPane.setCaretPosition(0);
		
		JDialog dialog = new JDialog(ShoppingViewWithMicroServices.this, "Orders", ModalityType.MODELESS);
		dialog.setPreferredSize(new Dimension(HEIGHT, HEIGHT));
		dialog.getContentPane().add(new JScrollPane(textPane));
		dialog.setLocationByPlatform(true);
		dialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		dialog.pack();
		dialog.setVisible(true);
	}	
}
