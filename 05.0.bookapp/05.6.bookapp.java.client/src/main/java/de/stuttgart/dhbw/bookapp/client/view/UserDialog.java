package de.stuttgart.dhbw.bookapp.client.view;

import static de.stuttgart.dhbw.bookapp.client.view.UserDialog.Entry.*;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class UserDialog {
		
	public static enum Entry {
		
		FIRSTNAME("Firstname", s -> nonEmptyText(s, 30), "Text must be non-empty and max. 30 characters."),
		LASTNAME("Lastname", s -> nonEmptyText(s, 30), "Text must be non-empty and max. 30 characters."),
		STREET("Street", s -> nonEmptyText(s, 100), "Text must be non-empty and max. 100 characters."),
		CITY("City", s -> nonEmptyText(s, 100), "Text must be non-empty and max. 100 characters."),
		EMAIL("Email", s -> validEmail(s), "Text must be a valid e-mail-address."), 
		USERNAME("Username", s -> validUsername(s), "Text must be non-empty and max. 30 characters and only consist of letters, digits and '.'."),
		PASSWORD("Password", s -> nonEmptyPassword(s, 6, 30), "Text must be non-empty, min. 6 and max. 30 characters.", true),
		NEW_PASSWORD("New Password", s -> nonEmptyPassword(s, 6, 30), "Text must be non-empty, min. 6 and max. 30 characters.", true);
		
		private String display;
		private Predicate<String> constraint;
		private Predicate<char[]> pwConstraint;
		private String tooltip;
		
		private Entry(String display, Predicate<String> constraint, String tooltip) {
			this.display = display;
			this.constraint = constraint;
			this.tooltip = tooltip;
		}
		
		private Entry(String display, Predicate<char[]> pwConstraint, String tooltip, boolean dummy) {
			this.display = display;
			this.pwConstraint = pwConstraint;
			this.tooltip = tooltip;
		}
		
		public boolean definesText() {
			return !definesPassword();
		}
		
		public boolean definesPassword() {
			return this == PASSWORD || this == NEW_PASSWORD;
		}
		
		public String display() {
			return display;
		}

		public Predicate<String> constraint() {
			return constraint;
		}
		
		public Predicate<char[]> pwConstraint() {
			return pwConstraint;
		}
		
		public String tooltip() {
			return tooltip;
		}

		private static boolean nonEmptyText(String str, int max) {
			return str != null && !str.isBlank() && str.length() <= max;
		}
		
		private static boolean nonEmptyPassword(char[] password, int min, int max) {
			return password != null && !isBlank(password) && password.length >= min && password.length <= max;
		}
		
		public static boolean isBlank(char[] password) {
			if(password.length == 0)
				return true;
			for(char c : password)
				if(!Character.isWhitespace(c))
					return false;
			return true;
		}
		
		private static boolean validEmail(String email) {
			if(!nonEmptyText(email, 320))
				return false;
			try {
				InternetAddress emailAddr = new InternetAddress(email);
				emailAddr.validate();
			} catch (AddressException ex) {
			   	return false;
			}
			return true;
		}
		
		private static boolean validUsername(String username) {
			if(!nonEmptyText(username, 30))
				return false;
			if(!Pattern.matches("[a-zA-Z0-9\\.]+", username))
				return false;
			return true;
		}		

	}

	@FunctionalInterface
	public static interface Procedure {
		//see https://stackoverflow.com/questions/23868733/java-8-functional-interface-with-no-arguments-and-no-return-value
	    void run();

	    default Procedure andThen(Procedure after){
	        return () -> {
	            this.run();
	            after.run();
	        };
	    }

	    default Procedure compose(Procedure before){
	        return () -> {
	            before.run();
	            this.run();
	        };
	    }
	}

	private static class EntryHolder {
		
		private boolean holdsText; // and not password
		private String text;
		private char[] password;
		
		EntryHolder(String text) {
			this.holdsText = true;
			this.text = text;
		}

		EntryHolder(char[] password) {
			this.holdsText = false;
			this.password = password;
		}
		
		public boolean holdsText() {
			return holdsText;
		}
		
		public boolean holdsPassword() {
			return !holdsText;
		}
		
		public String getText() {
			if(!holdsText())
				throw new NullPointerException();
			return text;
		}

		public char[] getPassword() {
			if(!holdsPassword())
				throw new NullPointerException();
			return password;
		}

		@Override
		public String toString() {
			return holdsText ? text : "******";
		}
	}
	
	public static class Entries {
		
		private Map<Entry, EntryHolder> map = new HashMap<>();
		
		public EntryHolder put(Entry key, String value) {
			return map.put(key, new EntryHolder(value));
		}
		
		public EntryHolder put(Entry key, char[] value) {
			return map.put(key, new EntryHolder(value));
		}
		
		public String get(Entry key) {
			EntryHolder holder = map.get(key);
			if(holder == null)
				return null;
			else
				return holder.getText();
		}
		
		public char[] getPw(Entry key) {
			EntryHolder holder = map.get(key);
			if(holder == null)
				return null;
			else
				return holder.getPassword();
		}

		@Override
		public String toString() {
			return map.toString();
		}	
	}
	
	@SuppressWarnings("serial")
	private static abstract class Field<FT extends JTextField, CT> extends JPanel {
		
		private static final Color BG = UIManager.getColor("TextField.background");

		private FT field;
		private Entry entry;
		private Predicate<CT> constraint = s -> true;
		private Procedure revalidate = () -> {};
		private boolean validState = true;

		Field(Class<FT> cls, int columns, String preset, Entry entry, String tooltip, 
				Predicate<CT> constraint, Procedure revalidate){
			this.setLayout(new GridLayout(1, 1));
			try {
				this.field = cls.getDeclaredConstructor(int.class).newInstance(columns);
				if(preset != null)
					this.field.setText(preset);
				this.add(field);
				this.entry = entry;
				if(revalidate != null)
					addValidation(tooltip, constraint, revalidate);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e1) {
				e1.printStackTrace();
			}			
		}
		
		public void addValidation(String tooltip, Predicate<CT> constraint, Procedure revalidate) {
			this.constraint = constraint;
			this.revalidate = revalidate;
			this.field.setToolTipText(tooltip);
			
			this.field.getDocument().addDocumentListener(new DocumentListener() {				
				@Override public void removeUpdate(DocumentEvent e)  { check(); }
				@Override public void insertUpdate(DocumentEvent e)  { check(); }
				@Override public void changedUpdate(DocumentEvent e) { check(); }
			});
			
			this.check();
		}
		
		protected FT getField() {
			return field;
		}
		
		public Entry getEntry() {
			return entry;
		}
		
		public boolean hasValidState() {
			return validState;
		}
		
		public abstract CT getText();
		
		public boolean isPasswordField() {
			return field instanceof JPasswordField;
		}
		
		@SuppressWarnings("unused")
		public boolean isTextField() {
			return !isPasswordField();
		}

		void check() {
			validState = constraint.test(this.getText());			
			if(validState)
				field.setBackground(BG);
			else
				field.setBackground(Color.RED);
			revalidate.run();
		}
	}
	
	@SuppressWarnings("serial")
	private static class TextField extends Field<JTextField, String>{

		private TextField(Class<JTextField> cls, int columns, String preset, Entry entry, String tooltip, Predicate<String> constraint, Procedure revalidate) {
			super(cls, columns, preset, entry, tooltip, constraint, revalidate);
		}

		public static TextField with(int columns, String preset, Entry entry, Procedure revalidate) {
			if(!entry.definesText())
				throw new RuntimeException("Invalid entry type: " + entry);
			
			return new TextField(JTextField.class, columns, preset,
					entry, entry.tooltip(), entry.constraint(), revalidate);		
		}

		@Override
		public String getText() {
			return this.getField().getText();
		}
	}

	@SuppressWarnings("serial")
	private static class PasswordField extends Field<JPasswordField, char[]>{

		private PasswordField(Class<JPasswordField> cls, int columns, Entry entry, String tooltip, Predicate<char[]> constraint, Procedure revalidate) {
			super(cls, columns, null, entry, tooltip, constraint, revalidate);
		}
		
		public static PasswordField with(int columns, Entry entry, Procedure revalidate) {
			if(!entry.definesPassword())
				throw new RuntimeException("Invalid entry type: " + entry);
			
			return new PasswordField(JPasswordField.class, columns, 
					entry, entry.tooltip(), entry.pwConstraint(), revalidate);		
		}

		@Override
		public char[] getText() {
			return this.getField().getPassword();
		}
	}
	
	@SuppressWarnings("serial")
	static class FormPanel extends JPanel {
		
		private int row = 0;
		private int columns;
		private Component first;
		private Procedure revalidate = () -> {};
				
		private List<TextField> textFields = new ArrayList<>();
		private List<PasswordField> passwordFields = new ArrayList<>();
		
		public static FormPanel columns(int columns) {
			return new FormPanel(columns, null);
		}
		
		public static FormPanel columns(int columns, Consumer<Boolean> onChange) {
			return new FormPanel(columns, onChange);
		}
		
		private FormPanel(int columns, Consumer<Boolean> onChange){
			super(new GridBagLayout());
			this.columns = columns;
			if(onChange != null)
				this.revalidate = () -> onChange.accept(this.hasValidState());
		}
		
		FormPanel check(Entry entry) {
			return check(entry, null);
		}
		
		FormPanel check(Entry entry, String preset) {
			if(entry.definesText())
				return addField(entry.display(), TextField.with(columns, preset, entry, revalidate));
			else
				return addField(entry.display(), PasswordField.with(columns, entry, revalidate));
		}
		
		FormPanel just(Entry entry) {
			if(entry.definesText())
				return addField(entry.display(), TextField.with(columns, null, entry, null));
			else
				return addField(entry.display(), PasswordField.with(columns, entry, null));
		}
		
		FormPanel checkTwice(Entry entry) { // Password pair
			if(!entry.definesPassword())
				throw new RuntimeException("Invalid entry type: " + entry);
		
			PasswordField pw1 = PasswordField.with(columns, entry, null);
			addComponent(entry.display(), pw1, row++);
			passwordFields.add(pw1);
			
			PasswordField pw2 = PasswordField.with(columns, entry, null);
			addComponent(entry.display() + " (agn.)", pw2, row++);
			passwordFields.add(pw2);
			
			pw1.addValidation(entry.tooltip(), 
					s -> { pw2.check(); return entry.pwConstraint().test(s);}, revalidate);
			pw2.addValidation("Text must be the same as above password.", 
					s -> !Entry.isBlank(s) && Arrays.equals(s, pw1.getText()), revalidate);
		
			revalidate.run();
			
			return this;
		}

		private boolean hasValidState() {
			for(Field<?, ?> field : textFields)
				if(!field.hasValidState())
					return false;
			for(Field<?, ?> field : passwordFields)
				if(!field.hasValidState())
					return false;
			return true;
		}
		
		private FormPanel addField(String label, TextField field) {
			textFields.add(field);
			addComponent(label, field);
			return this;
		}

		private FormPanel addField(String label, PasswordField field) {
			passwordFields.add(field);
			addComponent(label, field);
			return this;
		}

		private FormPanel addComponent(String label, Component comp) {
			if(first == null)
				first = comp;
			addComponent(label, comp, row++);
			revalidate.run();
			return this;
		}

		private void addComponent(String label, Component comp, int row) {
			GridBagConstraints constraints = new GridBagConstraints();
			constraints.insets = new Insets(1, 1, 1, 1);
			constraints.fill = GridBagConstraints.BOTH;
			constraints.gridy = row;
			
			JPanel labelPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
			labelPanel.add(new JLabel(label));
			
			constraints.gridx = 0;
			this.add(labelPanel, constraints);
			constraints.gridx = 1;
			this.add(comp, constraints);
		}
		
		Component getFirst() {
			return first;
		}

		Entries getEntries(){
			Entries entries = new Entries();
			for(TextField field : textFields)
				entries.put(field.getEntry(), field.getText());
			for(PasswordField field : passwordFields)
				entries.put(field.getEntry(), field.getText());
			return entries;
		}		
		
}
	
    private JOptionPane getOptionPane(JComponent parent) {
        if (parent instanceof JOptionPane)
        	return (JOptionPane) parent;
        else
        	return getOptionPane((JComponent)parent.getParent());
    }

    private JOptionPane getOptionPane(ActionEvent event) {
    	return this.getOptionPane((JComponent)event.getSource());
	}
    
    private JButton newButton(String title) {
    	final JButton button = new JButton(title);
    	button.addActionListener(e -> getOptionPane(e).setValue(button));
    	return button;
    }
    
    /// Actual dialogs /////////////////////////////////////////////////////

    public Entries registerDialog(Component parent) {
		// see: https://stackoverflow.com/questions/14334931/disable-ok-button-on-joptionpane-dialog-until-user-gives-an-input
		final JButton okay = newButton("OK");
		final JButton cancel = newButton("Cancel");
		
		FormPanel form = FormPanel.columns(30, v -> okay.setEnabled(v))
				.check(FIRSTNAME).check(LASTNAME).check(STREET).check(CITY).check(EMAIL)
				.check(USERNAME).checkTwice(PASSWORD);
		
		int result = JOptionPane.showOptionDialog(parent, form, "Register", JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, new Object[] { okay, cancel }, form.getFirst());
		
		if(result == JOptionPane.OK_OPTION)
			return form.getEntries();
		else
			return null;
	}
	

	public Entries loginDialog(Component parent) {
		final JButton okay = newButton("Ok");
		final JButton cancel = newButton("Cancel");
		
		FormPanel form = FormPanel.columns(30, v -> okay.setEnabled(v))
				.just(USERNAME).just(PASSWORD);

		int result = JOptionPane.showOptionDialog(parent, form, "Login", JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, new Object[] { okay, cancel }, form.getFirst());
		
		if(result == JOptionPane.OK_OPTION)
			return form.getEntries();
		else
			return null;
	}

	public Entries updateDataDialog(Component parent, String firstname, String lastname, String street,
			String city, String email) {

		final JButton okay = newButton("Ok");
		final JButton cancel = newButton("Cancel");
		
		FormPanel form = FormPanel.columns(30, v -> okay.setEnabled(v))
				.check(FIRSTNAME, firstname).check(LASTNAME, lastname)
				.check(STREET, street).check(CITY, city).check(EMAIL, email);
		
		int result = JOptionPane.showOptionDialog(parent, form, "Update User Data", JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, new Object[] { okay, cancel }, form.getFirst());
		
		if(result == JOptionPane.OK_OPTION)
			return form.getEntries();
		else
			return null;
	}

	public Entries updatePasswordDialog(Component parent) {

		final JButton okay = newButton("Ok");
		final JButton cancel = newButton("Cancel");
		
		FormPanel form = FormPanel.columns(30, v -> okay.setEnabled(v))
				.just(PASSWORD)
				.checkTwice(NEW_PASSWORD);
		
		int result = JOptionPane.showOptionDialog(parent, form, "Change Password", JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, new Object[] { okay, cancel }, form.getFirst());
		
		if(result == JOptionPane.OK_OPTION)
			return form.getEntries();
		else
			return null;
	}

	public Entries cofirmDeletionDialog(Component parent) {

		final JButton okay = newButton("Ok");
		final JButton cancel = newButton("Cancel");
		
		FormPanel form = FormPanel.columns(30, v -> okay.setEnabled(v))
				.just(PASSWORD);
		
		int result = JOptionPane.showOptionDialog(parent, form, "Confirm Deletion of Account", JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, new Object[] { okay, cancel }, form.getFirst());
		
		if(result == JOptionPane.OK_OPTION)
			return form.getEntries();
		else
			return null;
	}
	
//	public static void main(String[] args) {
//		UserDialog dialog = new UserDialog();
//		Entries result = null;
////		result = dialog.registerDialog(null);
//		result = dialog.loginDialog(null);
////		result = dialog.updateDataDialog(null, "John", "Doe", "Somestreet 7A", "Sometown 1234", "john.doe@provider.com");
////		result = dialog.updatePasswordDialog(null);
////		result = dialog.cofirmDeletionDialog(null);
//
//		System.out.println(result);
//	}
}
