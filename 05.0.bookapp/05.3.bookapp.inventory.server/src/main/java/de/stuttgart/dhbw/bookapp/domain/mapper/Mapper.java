package de.stuttgart.dhbw.bookapp.domain.mapper;

import java.util.ArrayList;
import java.util.List;
//import java.util.stream.Collectors;

public abstract class Mapper<D, E> {
	
	public abstract D toDomain(E entity);

	public abstract E toEntity(D domain);
	
	public List<D> toDomain(List<E> entities){
//		return entities.stream().map(e -> map(e)).collect(Collectors.toList());
		List<D> elements = new ArrayList<>();
		for(E entity : entities)
			elements.add(toDomain(entity));
		return elements;
	}

	public List<E> toEntity(List<D> domainElements){
		List<E> elements = new ArrayList<>();
		for(D domain : domainElements)
			elements.add(toEntity(domain));
		return elements;
	}
}
