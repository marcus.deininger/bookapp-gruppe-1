package de.stuttgart.dhbw.bookapp.server.communication;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.stuttgart.dhbw.bookapp.domain.application.Application;
import de.stuttgart.dhbw.bookapp.domain.application.Slice;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Author;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Book;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Genre;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(
	    info = @Info(
	        title = "Inventory-App",
	        description = "A Web Service to browse books.",
	        version = "3.0"
	    )
	)

// Call Swagger with: http://localhost:8080/book-app/swagger-ui.html
// Call App with: http://localhost:8080/book-app/...

@RestController
public class AppController {

	private static Application app = new Application();

	@Operation(summary = "Welcome message", description = "Returns a welcome message.")
	@GetMapping(path="", produces = "text/plain")
	public String welcome() {
 	   //	Call with: http://localhost:8080/inventory-app
		return "Welcome to the Inventory-App!";
	}

//	@Operation(summary = "Return all authors", description = "Returns all authors in the collection.")
//    @GetMapping(path="authors", produces = "application/json")
//	public List<Author> authors() {
//		return app.getAuthors();
//	}

//	@Operation(summary = "Return all genres", description = "Returns all genres in the collection.")
//    @GetMapping(path="genres", produces = "application/json")
//	public List<Genre> genres() {
//		return app.getGenres();
//	}

	@Operation(summary = "Return all authors", description = "Returns all authors in the collection.")
	@GetMapping(path="authors", produces = "application/json")
	public List<Author> authors(@RequestParam Optional<Integer> from,
									@RequestParam Optional<Integer> to,
									@RequestParam("sort-field") Optional<String> field,
									@RequestParam("sort-direction") Optional<String> direction) {
		return app.getAuthors(Slice.of(from, to, field, direction));
	}

	@Operation(summary = "Return all genres", description = "Returns all genres in the collection.")
	@GetMapping(path="genres", produces = "application/json")
	public List<Genre> genres(@RequestParam Optional<Integer> from,
								@RequestParam Optional<Integer> to,
								@RequestParam("sort-field") Optional<String> field,
								@RequestParam("sort-direction") Optional<String> direction) {
		return app.getGenres(Slice.of(from, to, field, direction));
	}

	@Operation(summary = "Return a book by id", description = "Return a book with the given Book-Id.")
    @GetMapping(path="book/{bookId}", produces = "application/json")
	public Book bookById(@PathVariable("bookId") int bookId) {
		return app.getBook(bookId);
	}

	@Operation(summary = "Return all books by an author", description = "Return all books by an author with the given author-id.")
    @GetMapping(path="books/author/{authorId}", produces = "application/json")
	public List<Book> booksByAuthor(@PathVariable("authorId") int authorId) {
		return app.getBooksByAuthor(authorId);
	}

	@Operation(summary = "Return all books by genre", description = "Return all books by an author with the given genre-id.")
    @GetMapping(path="books/genre/{genreId}", produces = "application/json")
	public List<Book> booksByGenre(@PathVariable("genreId") int genreId) {
		return app.getBooksByGenre(genreId);
	}
    
	@Operation(summary = "Return all books by author and genre", description = "Return all books by an author with the given author- and genre-id.")
    @GetMapping(path="books", produces = "application/json")
	public List<Book> booksByAuthorAndGenre(@RequestParam Optional<Integer> authorId, 
											@RequestParam Optional<Integer> genreId,
											
											@RequestParam Optional<Integer> from,
											@RequestParam Optional<Integer> to,
											@RequestParam("sort-field") Optional<String> field,
											@RequestParam("sort-direction") Optional<String> direction) {
		List<Book> books;
		Slice slice = Slice.of(from, to, field, direction);
		if(authorId.isEmpty() && genreId.isEmpty())
			books = app.getBooks(slice);
		else if (authorId.isPresent() && genreId.isEmpty())
			books = app.getBooksByAuthor(authorId.get(), slice);
		else if (authorId.isEmpty() && genreId.isPresent())
			books = app.getBooksByGenre(genreId.get(), slice);
		else // (authorId.isPresent() && genreId.isPresent())
			books = app.getBooksByAuthorAndGenre(authorId.get(), genreId.get(), slice);
		return books;
	}
	
	@Operation(summary = "Return the number of authors", description = "Return the number of authors.")
    @GetMapping(path="count/authors", produces = "application/json")
	public int numberOfAuthors() {
		return app.getNumberOfAuthors();
	}
	
	@Operation(summary = "Return the number of genres", description = "Return the number of genres.")
    @GetMapping(path="count/genres", produces = "application/json")
	public int numberOfGenres() {
		return app.getNumberOfGenres();
	}
	
	@Operation(summary = "Return the number of books by author and genre", description = "Return the number of books by an author with the given author- and genre-id.")
    @GetMapping(path="count/books", produces = "application/json")
	public int numberOfBooksByAuthorAndGenre(@RequestParam Optional<Integer> authorId, 
												@RequestParam Optional<Integer> genreId) {
		int number = 0;
		if(authorId.isEmpty() && genreId.isEmpty())
			number = app.getNumberOfBooks();
		else if (authorId.isPresent() && genreId.isEmpty())
			number = app.getNumberOfBooksByAuthor(authorId.get());
		else if (authorId.isEmpty() && genreId.isPresent())
			number = app.getNumberOfBooksByGenre(genreId.get());
		else // (authorId.isPresent() && genreId.isPresent())
			number = app.getNumberOfBooksByAuthorAndGenre(authorId.get(), genreId.get());
		return number;
	}
}
