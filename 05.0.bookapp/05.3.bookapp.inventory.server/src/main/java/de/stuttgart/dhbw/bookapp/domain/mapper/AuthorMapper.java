package de.stuttgart.dhbw.bookapp.domain.mapper;

import de.stuttgart.dhbw.bookapp.db.entities.AuthorEntity;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Author;

public class AuthorMapper extends Mapper<Author, AuthorEntity> {

	@Override
	public Author toDomain(AuthorEntity entity) {
		return new Author(entity.getId(), entity.getFirstname(), entity.getLastname());
	}

	@Override
	public AuthorEntity toEntity(Author author) {
		return new AuthorEntity(author.getId(), author.getFirstname(), author.getLastname());
	}
}
