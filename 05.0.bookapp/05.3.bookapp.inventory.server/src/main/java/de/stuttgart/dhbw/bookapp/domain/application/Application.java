package de.stuttgart.dhbw.bookapp.domain.application;

import java.util.List;

import de.stuttgart.dhbw.bookapp.db.access.InventoryDBAccess;
import de.stuttgart.dhbw.bookapp.domain.filter.BookFilter;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Author;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Book;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Genre;

public class Application {
	
	private static InventoryDBAccess inventoryDB = new InventoryDBAccess();
	
	private static final BookFilter BF = new BookFilter();
	
	public List<Author> getAuthors() {
		return inventoryDB.getAuthors();
	}

	public List<Genre> getGenres() {
		return inventoryDB.getGenres();
	}

	public List<Book> getBooks() {
		return BF.toClient(inventoryDB.getBooks());
	}
	
	public Book getBook(int bookId) {
		return inventoryDB.getBook(bookId);
	}

	public List<Book> getBooksByAuthor(Author author) {
		return this.getBooksByAuthor(author.getId());
	}

	public List<Book> getBooksByAuthor(int authorId) {
		return BF.toClient(inventoryDB.getBooksByAuthor(authorId));
	}

	public List<Book> getBooksByGenre(Genre genre) {
		return this.getBooksByGenre(genre.getId());
	}

	public List<Book> getBooksByGenre(int genreId) {
		return BF.toClient(inventoryDB.getBooksByGenre(genreId));
	}
	
	public List<Book> getBooksByAuthorAndGenre(Author author, Genre genre) {
		return this.getBooksByAuthorAndGenre(author.getId(), genre.getId());
	}

	public List<Book> getBooksByAuthorAndGenre(int authorId, int genreId) {
		return BF.toClient(inventoryDB.getBooksByAuthorAndGenre(authorId, genreId));
	}
	
	// Numbers
	
	public int getNumberOfAuthors() {
		return inventoryDB.getNumberOfAuthors();
	}

	public int getNumberOfGenres() {
		return inventoryDB.getNumberOfGenres();
	}

	public int getNumberOfBooks() {
		return inventoryDB.getNumberOfBooks();
	}
	
	public int getNumberOfBooksByAuthor(int authorId) {
		return inventoryDB.getNumberOfBooksByAuthor(authorId);
	}
	
	public int getNumberOfBooksByGenre(int genreId) {
		return inventoryDB.getNumberOfBooksByGenre(genreId);
	}
	
	public int getNumberOfBooksByAuthorAndGenre(int authorId, int genreId) {
		return inventoryDB.getNumberOfBooksByAuthorAndGenre(authorId, genreId);
	}
	
	// Slicing

	public List<Author> getAuthors(Slice slice) {
		return slice.ofAuthors(getAuthors());
	}

	public List<Genre> getGenres(Slice slice) {
		return slice.ofGenres(getGenres());
	}

	public List<Book> getBooks(Slice slice) {
		return BF.toClient(slice.ofBooks(getBooks()));
	}

	public List<Book> getBooksByAuthor(int authorId, Slice slice) {
		return BF.toClient(slice.ofBooks(getBooksByAuthor(authorId)));
	}

	public List<Book> getBooksByGenre(int genreId, Slice slice) {
		return BF.toClient(slice.ofBooks(getBooksByGenre(genreId)));
	}

	public List<Book> getBooksByAuthorAndGenre(int genreId, int authorId, Slice slice) {
		return BF.toClient(slice.ofBooks(getBooksByAuthorAndGenre(authorId, genreId)));
	}
	
}
