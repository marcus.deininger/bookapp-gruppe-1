package de.stuttgart.dhbw.bookapp.domain.mapper;

import de.stuttgart.dhbw.bookapp.db.entities.BookEntity;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Book;

public class BookMapper extends Mapper<Book, BookEntity> {
	
	private static final AuthorMapper AM = new AuthorMapper();
	private static final GenreMapper GM = new GenreMapper();
	
	@Override
	public Book toDomain(BookEntity entity) {
		return new Book(entity.getId(), 
				AM.toDomain(entity.getAuthor()), GM.toDomain(entity.getGenre()),
				entity.getTitle(), entity.getYear(), entity.getSample());
	}


	@Override
	public BookEntity toEntity(Book book) {
		return new BookEntity(book.getId(), 
				AM.toEntity(book.getAuthor()), GM.toEntity(book.getGenre()),
				book.getTitle(), book.getYear(), book.getSample());
	}
}
