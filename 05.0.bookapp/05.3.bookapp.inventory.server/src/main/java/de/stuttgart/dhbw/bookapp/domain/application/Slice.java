package de.stuttgart.dhbw.bookapp.domain.application;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.stuttgart.dhbw.bookapp.inventory.domain.model.Author;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Book;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Genre;

import static java.util.stream.Collectors.*;

import static de.stuttgart.dhbw.bookapp.domain.application.Slice.OrderField.*;
import static de.stuttgart.dhbw.bookapp.domain.application.Slice.OrderDirection.*;

public class Slice {
	
	public static int MAX_SLICE = 100;
	
	static enum OrderField {
		TITLE("title"), AUTHOR("author"), GENRE("genre"), YEAR("year"), UNSET ;
		
		private OrderField() {
			this.pattern = null;
		}

		private OrderField(String regex) {
			this.pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		}

		private Pattern pattern;
		
		private boolean matches(String str) {
			if(pattern == null)
				return true;
			else
				return pattern.matcher(str).matches();
		}
		
		public static OrderField get(String str) {
			if(str == null || str.isBlank())
				return UNSET;
			for(OrderField field : OrderField.values()) {
				if(field.matches(str))
					return field;
			}
			return UNSET;
		}
	}

	static enum OrderDirection {
		ASCENDING("as.*"), DESCENDING("de.*"), UNORDERED;
		
		private OrderDirection() {
			this.pattern = null;
		}

		private OrderDirection(String regex) {
			this.pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		}

		private Pattern pattern;
		
		private boolean matches(String str) {
			if(pattern == null)
				return true;
			else
				return pattern.matcher(str).matches();
		}
		
		public static OrderDirection get(String str) {
			if(str == null || str.isBlank())
				return UNORDERED;
			for(OrderDirection order : OrderDirection.values()) {
				if(order.matches(str))
					return order;
			}
			return UNORDERED;
		}
	}

	private static Application app = new Application();
	
	private boolean allTo = false;
	private int from, to;
	private OrderField field;
	private OrderDirection direction;
	boolean ascending;
	
	public static Slice of(Optional<Integer> from, Optional<Integer> to, Optional<String> field, Optional<String> direction) {
		return new Slice(from, to, field, direction);
	}

	private Slice(Optional<Integer> from, Optional<Integer> to, Optional<String> field, Optional<String> direction) {
		if(from.isEmpty())
			this.from = 0;
		else
			this.from = (from.get() <= 0 ? 0 : from.get() - 1);
			
		if(to.isEmpty())
			this.allTo = true;
		else
			this.to = (to.get() < this.from ? this.from : to.get());
		
		if(field.isEmpty())
			this.field = UNSET;
		else
			this.field = OrderField.get(field.get());
		
		if(direction.isEmpty())
			this.direction = UNORDERED;
		else
			this.direction = OrderDirection.get(direction.get());
		
		if(this.field == UNSET || this.direction == UNORDERED) {
			this.field = UNSET;
			this.direction = UNORDERED;
		}
		this.ascending = this.direction == ASCENDING;
	}
	
	List<Author> ofAuthors(List<Author> authors) {
		return ofAuthors(authors, direction, false);
	}

	private List<Author> ofAuthors(List<Author> authors, OrderDirection direction, boolean all) {
		
		if(direction != UNORDERED) {
			int dir = ascending ? 1 : -1;
			authors.sort((Author a1, Author a2) -> {
				int r = a1.getLastname().compareTo(a2.getLastname());
				if(r == 0) r = a1.getFirstname().compareTo(a2.getFirstname());
				r = r * dir;
				return r;
			});
		}
		
		if(all)
			return authors;

		int to = this.to;
		if(allTo || to > authors.size())
			to = authors.size();
		
		authors = authors.subList(from, to);
		return authors;
	}


	List<Genre> ofGenres(List<Genre> genres) {
		return ofGenres(genres, direction, false);
	}

	private List<Genre> ofGenres(List<Genre> genres, OrderDirection direction, boolean all) {
		
		if(direction != UNORDERED) {	
			int dir = ascending ? 1 : -1;
			genres.sort((Genre g1, Genre g2) -> g1.getGenreName().compareTo(g2.getGenreName()) * dir);
		}
		
		if(all)
			return genres;
		
		int to = this.to;
		if(allTo || to > genres.size())
			to = genres.size();
		
		genres = genres.subList(from, to);
		return genres;
	}

	List<Book> ofBooks(List<Book> books) {
				
		switch(field) {
		case TITLE:
			Pattern pattern = Pattern.compile("(The|An|A)\\s+(?<title>.*)", Pattern.CASE_INSENSITIVE);
			Function<Book, String> title = b -> { 
				Matcher m = pattern.matcher(b.getTitle()); 
				return m.matches() ? m.group("title") : b.getTitle();
			};
			int dir = ascending ? 1 : -1;
			books.sort((b1, b2) -> title.apply(b1).compareTo(title.apply(b2)) * dir);
			break;
			
		case AUTHOR:
			List<Author> authors = ofAuthors(app.getAuthors(), direction, true);	
			Map<Integer, Integer> authorMap = new HashMap<>();
			for(int i = 0; i < authors.size(); i++)
				authorMap.put(authors.get(i).getId(), i);
			
			books.sort((b1, b2) -> authorMap.get(b1.getAuthor().getId()) - authorMap.get(b2.getAuthor().getId()));
			break;
			
		case GENRE:
	        List<Genre> genres = ofGenres(app.getGenres(), direction, true);        	
			Map<Integer, Integer> genreMap = new HashMap<>();
			for(int i = 0; i < genres.size(); i++)
				genreMap.put(genres.get(i).getId(), i);
			books.sort((b1, b2) -> genreMap.get(b1.getGenre().getId()) - genreMap.get(b2.getGenre().getId()));
			break;
			
		case YEAR:
			books.sort((b1, b2) -> b1.getYear() - b2.getYear());
			break;
			
		default: // Do Nothing
			break;
		}
		
		int to = this.to;
		if(allTo || to > books.size())
			to = books.size();
		
		books = books.subList(from, to);
		if(to - from <= MAX_SLICE) { // refresh
			Map<Integer, Author> authorMap = app.getAuthors().stream().collect(
				      toMap(a -> a.getId(), Function.identity()));
			
			Map<Integer, Genre> genreMap = app.getGenres().stream().collect(
				      toMap(g -> g.getId(), Function.identity()));

			for(Book book : books) {
				int authorId = book.getAuthor().getId();
				book.setAuthor(authorMap.get(authorId));

				int genreId = book.getGenre().getId();
				book.setGenre(genreMap.get(genreId));
	        }
		}

		return books;
	}
	
}
