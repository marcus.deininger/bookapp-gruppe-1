package de.stuttgart.dhbw.bookapp.domain.mapper;

import de.stuttgart.dhbw.bookapp.db.entities.GenreEntity;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Genre;

public class GenreMapper extends Mapper<Genre, GenreEntity> {

	@Override
	public Genre toDomain(GenreEntity entity) {
		return new Genre(entity.getId(), entity.getGenreName());
	}

	@Override
	public GenreEntity toEntity(Genre genre) {
		return new GenreEntity(genre.getId(), genre.getGenreName());
	}

}
