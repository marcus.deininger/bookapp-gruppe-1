package de.stuttgart.dhbw.bookapp.domain.filter;

import de.stuttgart.dhbw.bookapp.inventory.domain.model.Book;

public class BookFilter extends Filter<Book> {

	@Override
	public Book toClient(Book book) {
		return new Book(book.getId(), book.getAuthor(), book.getGenre(), book.getTitle(), book.getYear(), null);
	}
}
