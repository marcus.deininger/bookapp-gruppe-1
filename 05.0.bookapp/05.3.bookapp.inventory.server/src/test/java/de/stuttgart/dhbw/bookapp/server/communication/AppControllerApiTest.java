package de.stuttgart.dhbw.bookapp.server.communication;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import de.stuttgart.dhbw.bookapp.db.entities.AuthorEntity;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Author;
import de.stuttgart.dhbw.bookapp.test.util.DbUrl;

//see: https://www.arhohuttunen.com/spring-boot-webmvctest/
//see: https://stackoverflow.com/questions/18336277/how-to-check-string-in-response-body-with-mockmvc

@WebMvcTest(AppController.class)
@ContextConfiguration(classes = AppController.class)  // This is annoyingly important!
public class AppControllerApiTest {

	@Autowired
	private MockMvc mockMvc;

	private static final String DB_URL = DbUrl.get();
	
	private static Map<Integer, AuthorEntity> entities;
	private static List<AuthorEntity> samples = List.of(
					new AuthorEntity(0, "Lokesh", "Gupta"), 
					new AuthorEntity(0, "Alex", "Gussin"));

	@BeforeAll
	static void setUp() {
		try (ConnectionSource connectionSource = new JdbcConnectionSource(DB_URL)) {
			Dao<AuthorEntity, Integer> authorDao = DaoManager.createDao(connectionSource, AuthorEntity.class);

			TableUtils.createTableIfNotExists(connectionSource, AuthorEntity.class);
			TableUtils.clearTable(connectionSource, AuthorEntity.class);

			entities = new HashMap<>();
			for(AuthorEntity entity : samples) {
				authorDao.create(entity);
				entities.put(entity.getId(), entity);
			}

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	void testWelcome() throws Exception {

		//			MvcResult result = mockMvc.perform(get("/"))
		//			    .andExpect(status().isOk())
		//			    .andReturn();

		//			String content = result.getResponse().getContentAsString();
		//			System.out.println(content);

		mockMvc.perform(get("/"))
		.andExpect(status().isOk())
		.andExpect(content().string("Welcome to the Inventory-App!"));
	}

	private List<Author> toAuthorList(String jsonString){
		List<Author> authors = new ArrayList<>();
		try {
			JSONArray jsonArray = new JSONArray(jsonString);
//			System.out.println(jsonArray);
			for(int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);

				int id = jsonObject.getInt("id");
				String firstname = jsonObject.getString("firstname");
				String lastname = jsonObject.getString("lastname");

				Author author = new Author(id, firstname, lastname);
				authors.add(author);
			}
		} catch (JSONException e) {
			e.printStackTrace();
			return new ArrayList<>();
		}

		return authors;
	}

	private  void verifyEntity(Author expected, AuthorEntity entity) {
		assertEquals(expected.getId(), entity.getId(), "expected id does not equal id");
		assertEquals(expected.getFirstname(), entity.getFirstname(), "expected first name does not equal author name");
		assertEquals(expected.getLastname(), entity.getLastname(), "expected last name does not equal author name");
	}

	@Test
	void testAuthors() throws Exception {
		MvcResult result = mockMvc.perform(get("/authors"))
				.andExpect(status().isOk())
				.andReturn();

		String content = result.getResponse().getContentAsString();
		List<Author> authors = toAuthorList(content);
		System.out.println(authors);
		
		assertEquals(entities.size(), authors.size(), "Should have found same number of authors in map");
		for (Author author : authors) {
			assertTrue(entities.containsKey(author.getId()), "Should have found account in map");
			verifyEntity(author, entities.get(author.getId()));
		}
		System.out.println("testAuthors passed");
	}

	@Test
	void testNumberOfAuthors() throws Exception {
		MvcResult result = mockMvc.perform(get("/count/authors"))
				.andExpect(status().isOk())
				.andReturn();

		String content = result.getResponse().getContentAsString();
		int number = Integer.parseInt(content);
		assertEquals(entities.size(), number, "Should have found same number of authors in map");
	}
}
