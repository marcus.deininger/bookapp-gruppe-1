package de.stuttgart.dhbw.bookapp.test.util;

import java.io.InputStream;
import java.util.Properties;

public class DbUrl{
	
	private static final String		PROPERTIES_FILE = "db.properties";
	private static final String		DB_URL = "dburl";

	public static String get() {
		return getProperty(PROPERTIES_FILE, DB_URL);
	}
	
	private static String getProperty(String filePath, String key) {

        Properties properties = new Properties();

        try (InputStream resourceAsStream = DbUrl.class.getClassLoader().getResourceAsStream(filePath)) {
            properties.load(resourceAsStream);

//            System.out.println("Properties");
//            prop.forEach((k, v) -> System.out.println(k + " -> " + v));

        } catch (Exception e) {
            System.err.println("Unable to load properties file : " + filePath);
        }        

        return properties.getProperty(key);

    }

}
