package de.stuttgart.dhbw.bookapp.domain.application;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import de.stuttgart.dhbw.bookapp.db.entities.AuthorEntity;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Author;
import de.stuttgart.dhbw.bookapp.test.util.DbUrl;

public class ApplicationUnitTest {

	private static final String DB_URL = DbUrl.get();
	
	private static Application application = new Application();
	private static Map<Integer, AuthorEntity> entities;
	private static List<AuthorEntity> samples = List.of(
					new AuthorEntity(0, "Lokesh", "Gupta"), 
					new AuthorEntity(0, "Alex", "Gussin"));

	@BeforeAll
	static void setUp() {
		try (ConnectionSource connectionSource = new JdbcConnectionSource(DB_URL)) {
			Dao<AuthorEntity, Integer> authorDao = DaoManager.createDao(connectionSource, AuthorEntity.class);

			TableUtils.createTableIfNotExists(connectionSource, AuthorEntity.class);
			TableUtils.clearTable(connectionSource, AuthorEntity.class);

			entities = new HashMap<>();
			for(AuthorEntity entity : samples) {
				authorDao.create(entity);
				entities.put(entity.getId(), entity);
			}

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	}
	
	private  void verifyEntity(Author expected, AuthorEntity entity) {
		assertEquals(expected.getId(), entity.getId(), "expected id does not equal id");
		assertEquals(expected.getFirstname(), entity.getFirstname(), "expected first name does not equal author name");
		assertEquals(expected.getLastname(), entity.getLastname(), "expected last name does not equal author name");
	}

	@Test
	void testAuthors() {
		List<Author> authors = application.getAuthors();
		System.out.println(authors);
		
		assertEquals(entities.size(), authors.size(), "Should have found same number of authors in map");
		for (Author author : authors) {
			assertTrue(entities.containsKey(author.getId()), "Should have found account in map");
			verifyEntity(author, entities.get(author.getId()));
		}
		System.out.println("testAuthors passed");
	}

	@Test
	void testNumberOfAuthors() {
		int number = application.getNumberOfAuthors();
		assertEquals(entities.size(), number, "Should have found same number of authors in map");
	}
}
