package de.stuttgart.dhbw.bookapp.sales.domain.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Order {
	
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("d.M.yyyy, H:mm");
	
	private int id;
	private Customer customer;
	private ItemType itemType;
	private int itemId;
	private LocalDateTime orderDate;
	
	private String header, body;
	
	public Order() {} // This is crucial for JSON

	public Order(int id, Customer customer, ItemType itemType, 
			int itemId, LocalDateTime orderDate, String header, String body) {
		this.id = id;
		this.customer = customer;
		this.itemType = itemType;
		this.itemId = itemId;
		this.orderDate = orderDate;
		this.header = header;
		this.body = body;
	}
	
	public static Order initialOrder(ItemType itemType, int itemId, String header, String body) {
		return new Order(0, null, itemType, itemId, null, header, body);
	}

	public int getId() {
		return id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public ItemType getItemType() {
		return itemType;
	}

	public int getItemId() {
		return itemId;
	}

	public LocalDateTime getOrderDate() {
		return orderDate;
	}

	public String getFormattedDate() {
		if(orderDate == null)
			return "<no date available>";
		else
			return orderDate.format(FORMATTER);
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", customer=" + customer + ", itemType=" + itemType + ", itemId=" + itemId
				+ ", orderDate=" + orderDate + "]";
	}

}
