package de.stuttgart.dhbw.bookapp.sales.domain.model;

public enum ItemType {
	
	BOOK(1), MEDIA(2), EQUIPMENT(3); // ...
	
	private int id;
	
	private ItemType(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
}
