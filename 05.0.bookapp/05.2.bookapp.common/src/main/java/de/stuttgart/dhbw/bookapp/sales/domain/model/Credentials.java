package de.stuttgart.dhbw.bookapp.sales.domain.model;

public class Credentials {
	
	private String username;
	private char[] password;
	private char[] newPassword;
	
	// needed for JSON
	public Credentials() {		
	}

	public Credentials(char[] password) {
		this.password = password;
	}

	public Credentials(String username, char[] password) {
		this.username = username;
		this.password = password;
	}

	public Credentials(char[] password, char[] newPassword) {
		this.password = password;
		this.newPassword = newPassword;
	}


	public String getUsername() {
		return username;
	}

	public char[] getPassword() {
		return password;
	}

	public char[] getNewPassword() {
		return newPassword;
	}
}
