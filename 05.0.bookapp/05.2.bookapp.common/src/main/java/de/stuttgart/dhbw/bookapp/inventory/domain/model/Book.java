package de.stuttgart.dhbw.bookapp.inventory.domain.model;

public class Book{

	private int id;	
	private Author author;
	private Genre genre;
	private String title;
	private int year;
	private String sample;

	// Required by JSON
	public Book() {}

	// Required by Mapper
	public Book(int id, Author author, Genre genre, String title, int year, String sample) {
		this.id = id;
		this.author = author;
		this.genre = genre;
		this.title = title;
		this.year = year;
		this.sample = sample;
	}

	public Book toClient() {
		return new Book(id, author, genre, title, year, null);
	}

	public int getId() {
		return id;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public String getTitle() {
		return title;
	}

	public int getYear() {
		return year;
	}
	
	public String getSample() {
		return sample;
	}
	
	public void setSample(String sample) {
		this.sample = sample;
	}

	@Override
	public String toString() {
		return title + " (" + year + ")";
	}
}
