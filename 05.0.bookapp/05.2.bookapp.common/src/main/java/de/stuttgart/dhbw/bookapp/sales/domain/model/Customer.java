package de.stuttgart.dhbw.bookapp.sales.domain.model;

public class Customer {
	
	private int id;
	private String firstname;
	private String lastname;
	private String street;
	private String city;
	
	private String email;
	private String username;
	private String password; // just the hashed password
	
	private char[] plainPassword; // just for transfer;

	// Required by JSON
	public Customer() {}

	// Required for Mapper
	public Customer(int id, String firstname, String lastname, String street, String city, String email, String username, String password) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.street = street;
		this.city = city;
		this.email = email;
		this.username = username;
		this.password = password;
	}
	
	// needed for Client creation
	public static Customer newCustomer(String firstname, String lastname, String street, String city, String email, String username, char[] password) {
		Customer customer = new Customer(0, firstname, lastname, street, city, email, username, null);
		customer.plainPassword = password;
		return customer;
	}

	public static Customer updatedCustomer(String firstname, String lastname, String street, String city, String email) {
		return new Customer(0, firstname, lastname, street, city, email, null, null);
	}

	public int getId() {
		return id;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getStreet() {
		return street;
	}

	public String getCity() {
		return city;
	}

	public String getEmail() {
		return email;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public char[] getPlainPassword() {
		return plainPassword;
	}

	public Customer with(String password) {
		return new Customer(id, firstname, lastname, street, city, email, username, password);
	}

	@Override
	public String toString() {
		return username + "<" + email + ">";
	}
}
