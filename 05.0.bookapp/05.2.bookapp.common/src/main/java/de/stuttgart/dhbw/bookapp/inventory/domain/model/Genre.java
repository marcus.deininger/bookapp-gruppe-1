package de.stuttgart.dhbw.bookapp.inventory.domain.model;

public class Genre{

	private int id;
	private String genreName;

	// Required by JSON
	public Genre() {}

	// Required by Mapper
	public Genre(int id, String genreName) {
		super();
		this.id = id;
		this.genreName = genreName;
	}

	public int getId() {
		return id;
	}

	public String getGenreName() {
		return genreName;
	}

	@Override
	public String toString() {
		return genreName;
	}
}
