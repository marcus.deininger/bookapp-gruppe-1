package de.stuttgart.dhbw.bookapp.client.model;

public class IntHolder {
	
	public int value;
	
	@Override
	public String toString() {
		return "Holder [" + (value == 0 ? "" : value) + "]";
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
