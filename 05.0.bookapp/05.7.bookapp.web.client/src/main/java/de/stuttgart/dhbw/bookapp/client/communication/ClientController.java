package de.stuttgart.dhbw.bookapp.client.communication;

import static de.stuttgart.dhbw.bookapp.sales.domain.model.ItemType.BOOK;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import de.stuttgart.dhbw.bookapp.client.model.CustomerData;
import de.stuttgart.dhbw.bookapp.client.model.CustomerSession;
import de.stuttgart.dhbw.bookapp.client.model.IntHolder;
import de.stuttgart.dhbw.bookapp.client.server.communication.InventoryService;
import de.stuttgart.dhbw.bookapp.client.server.communication.SalesService;
import de.stuttgart.dhbw.bookapp.client.server.communication.SliceSettings;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Author;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Book;
import de.stuttgart.dhbw.bookapp.inventory.domain.model.Genre;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Credentials;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Customer;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Order;


@Controller
public class ClientController {

	private static final InventoryService INVENTORY_SERVICE = new InventoryService();
	private static final SalesService SALES_SERVICE = new SalesService();

	private <T> T get(HttpServletRequest request, String name, Supplier<T> supplier) {
		HttpSession session = request.getSession();

		@SuppressWarnings("unchecked")
		T t = (T) session.getAttribute(name);
		if (t != null)
			return t;

		t = supplier.get();

		session.setAttribute(name, t);
		return t;
	}

	private SliceSettings getSliceSettings(String name, HttpServletRequest request) {
		return get(request, name, SliceSettings::new);
	}

	private CustomerSession getCustomerSession(HttpServletRequest request) {
		return get(request, "customerSession", CustomerSession::new);
	}

	@RequestMapping("/")
	public String root(HttpServletRequest request, Model model) {
		getCustomerSession(request);
		return "index";
	}
	
	// Data /////////////////////////////////////////////////////////////

	@GetMapping("/books")
	public String books(HttpServletRequest request, 
				@RequestParam(name = "sort-field") Optional<String> field,
				@RequestParam(name = "page") Optional<Integer> page,
			Model model) {
		
		SliceSettings sliceSettings = getSliceSettings("booksSlice", request)
				.setCount(INVENTORY_SERVICE.getNumberOfBooks())
				.setPage(page).setField(field);
		model.addAttribute("books", INVENTORY_SERVICE.getBooks(sliceSettings));
		return "books";
	}

	@GetMapping("/books/author/{authorId}")
	public String booksByAuthor(HttpServletRequest request,
				@PathVariable("authorId") int authorId,
				@RequestParam(name = "sort-field") Optional<String> field,
				@RequestParam(name = "page") Optional<Integer> page,
			Model model) {

		SliceSettings sliceSettings = getSliceSettings("booksByAuthorSlice", request)
				.setCount(INVENTORY_SERVICE.getNumberOfBooksByAuthor(authorId))
				.setPage(page).setField(field);
		
		List<Book> books = INVENTORY_SERVICE.getBooksByAuthor(authorId, sliceSettings);
		model.addAttribute("books", books);

		Author author = null;
		if (!books.isEmpty())
			author = books.get(0).getAuthor();
		model.addAttribute("author", author);
		return "author";
	}

	@GetMapping("/books/genre/{genreId}")
	public String booksByGenre(HttpServletRequest request,
				@PathVariable("genreId") int genreId,
				@RequestParam(name = "sort-field") Optional<String> field,
				@RequestParam(name = "page") Optional<Integer> page,
			Model model) {
		
		SliceSettings sliceSettings = getSliceSettings("booksByGenreSlice", request)
				.setCount(INVENTORY_SERVICE.getNumberOfBooksByGenre(genreId))
				.setPage(page).setField(field);
		
		List<Book> books = INVENTORY_SERVICE.getBooksByGenre(genreId, sliceSettings);
		model.addAttribute("books", books);

		Genre genre = null;
		if (!books.isEmpty())
			genre = books.get(0).getGenre();
		model.addAttribute("genre", genre);
		return "genre";
	}

	@GetMapping(path = "/book/{bookId}")
	public String book(@PathVariable("bookId") int bookId, Model model) {
		model.addAttribute("book", INVENTORY_SERVICE.getBook(bookId));
		return "book";
	}

	@GetMapping("/authors")
	public String authors(HttpServletRequest request, 
						@RequestParam(name = "sort-field") Optional<String> field,
						@RequestParam(name = "page") Optional<Integer> page,
					Model model) {
		
		SliceSettings sliceSettings = getSliceSettings("authorsSlice", request)
				.setCount(INVENTORY_SERVICE.getNumberOfAuthors())
				.setPage(page).setField(field);
		
		List<Author> authors = INVENTORY_SERVICE.getAuthors(sliceSettings);

		model.addAttribute("authors", authors);
		return "authors";
	}

	@GetMapping("/genres")
	public String genres(HttpServletRequest request, 
					@RequestParam(name = "sort-field") Optional<String> field,
					@RequestParam(name = "page") Optional<Integer> page,
				Model model) {

		SliceSettings sliceSettings = getSliceSettings("genresSlice", request)
				.setCount(INVENTORY_SERVICE.getNumberOfGenres())
				.setPage(page).setField(field);
		
		List<Genre> genres = INVENTORY_SERVICE.getGenres(sliceSettings);
		model.addAttribute("genres", genres);
		return "genres";
	}

	// Account /////////////////////////////////////////////////////////////

	@GetMapping("/login")
	public String login(@ModelAttribute("credentials") CustomerData customer, Model model) {
		return "login";
	}

	@PostMapping("/login")
	public String loginSubmit(HttpServletRequest request, @ModelAttribute("credentials") CustomerData customer, Model model) {
		Credentials loginCredentials = customer.loginCredentials();
		if(loginCredentials == null)
			return "index";
		
		String sessionId = SALES_SERVICE.login(loginCredentials);
		if(sessionId == null)
			return "index";
		
		CustomerSession customerSession = getCustomerSession(request);
		customerSession.setUsernameAndSessionId(customer.getUsername(), sessionId);
		return "index";
	}

	@RequestMapping("/logout")
	public String logout(HttpServletRequest request, Model model) {
		CustomerSession customerSession = getCustomerSession(request);
		SALES_SERVICE.logout(customerSession.getSessionId());
		customerSession.logOut();
		return "index";
	}

	@GetMapping("/register")
	public String register(@ModelAttribute("customer") CustomerData customer, Model model) {
		return "register";
	}

	@PostMapping("/register")
	public String registerSubmit(@ModelAttribute("customer") CustomerData customer, HttpServletRequest request, Model model) {
		Customer newCustomer = customer.newCustomer();
		if(newCustomer == null)
			return "index";
		
		String sessionId = SALES_SERVICE.register(newCustomer);
		CustomerSession customerSession = getCustomerSession(request);
		customerSession.setUsernameAndSessionId(customer.getUsername(), sessionId);
		return "index";
	}

	@GetMapping("/update")
	public String update(HttpServletRequest request, @ModelAttribute("customer") CustomerData customer, Model model) {
		CustomerSession customerSession = getCustomerSession(request);
		String sessionId = customerSession.getSessionId();
		
		Customer storedCustomer = SALES_SERVICE.getCustomer(sessionId);
		customer.setFirstname(storedCustomer.getFirstname());
		customer.setLastname(storedCustomer.getLastname());
		customer.setStreet(storedCustomer.getStreet());
		customer.setCity(storedCustomer.getCity());
		customer.setEmail(storedCustomer.getEmail());
		
		return "account";
	}

	@PostMapping("/update")
	public String updateSubmit(HttpServletRequest request, @ModelAttribute("customer") CustomerData customer, Model model) {
		CustomerSession customerSession = getCustomerSession(request);
		Customer updatedCustomer = customer.updatedCustomer();
		SALES_SERVICE.updateUser(customerSession.getSessionId(), updatedCustomer);
		
		Credentials updatedCredentials = customer.updatedCredentials();
		if(updatedCredentials != null)
			SALES_SERVICE.updatePassword(customerSession.getSessionId(), updatedCredentials);
		
		return "index";
	}

	@GetMapping("/orders")
	public String orders(HttpServletRequest request, Model model) {
		List<Order> orders = SALES_SERVICE.getOrders(getCustomerSession(request).getSessionId(), BOOK);
		for(Order order : orders) {
			Book book = INVENTORY_SERVICE.getBook(order.getItemId());
			if(book != null)
				order.setHeader(book.getTitle());
		}
		
		orders.sort((o1, o2) -> -o1.getOrderDate().compareTo(o2.getOrderDate()));
		model.addAttribute("orders", orders);

		return "orders";
	}

	@PostMapping("/order")
	public String order(HttpServletRequest request, @ModelAttribute IntHolder holder, Model model) {
		CustomerSession customerSession = getCustomerSession(request);
		if (!customerSession.loggedIn())
			return "index";

		Book orderedBook = INVENTORY_SERVICE.getBook(holder.value);
		if(orderedBook == null)
			return "index";

		Order preparedOrder = Order.initialOrder(BOOK, orderedBook.getId(), 
				orderedBook.getTitle(), "'" + orderedBook.getTitle() + "' by " + orderedBook.getAuthor());
		boolean ok = SALES_SERVICE.placeOrder(customerSession.getSessionId(), preparedOrder);
		if (!ok)
			return "index";

		List<Order> orders = SALES_SERVICE.getOrders(getCustomerSession(request).getSessionId(), BOOK);
		for(Order order : orders) {
			Book book = INVENTORY_SERVICE.getBook(order.getItemId());
			if(book != null)
				order.setHeader(book.getTitle());
		}

		orders.sort((o1, o2) -> -o1.getOrderDate().compareTo(o2.getOrderDate()));
		model.addAttribute("orders", orders);
		return "orders";
	}

}