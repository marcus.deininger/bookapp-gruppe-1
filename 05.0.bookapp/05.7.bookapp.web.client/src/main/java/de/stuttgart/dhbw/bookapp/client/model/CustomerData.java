package de.stuttgart.dhbw.bookapp.client.model;

import java.util.Arrays;

import de.stuttgart.dhbw.bookapp.sales.domain.model.Credentials;
import de.stuttgart.dhbw.bookapp.sales.domain.model.Customer;

public class CustomerData {
	
	private String firstname;
	private String lastname;
	private String street;
	private String city;
	
     private String email;
	private String username;
	
	private char[] password;
	private char[] confirmPassword;
	private char[] oldPassword;
	
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public char[] getPassword() {
		return password;
	}

	public void setPassword(char[] password) {
		this.password = password;
	}

	public char[] getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(char[] confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public char[] getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(char[] oldPassword) {
		this.oldPassword = oldPassword;
	}
	
	public Customer updatedCustomer() {
		return Customer.updatedCustomer(firstname, lastname, street, city, email);
	}
	
	public Credentials loginCredentials() {
		if(username == null	|| username.isBlank())		return null;
		if(password == null	|| password.length == 0)	return null;
		
		return new Credentials(username, password);
	}

	public Customer newCustomer() {
		if(password == null			|| password.length == 0)		return null;
		if(confirmPassword == null	|| confirmPassword.length == 0)	return null;
		
		if(password.length != confirmPassword.length) return null;
		for (int i = 0; i < password.length; i++)
			if(password[i] != confirmPassword[i])
				 return null;

		return Customer.newCustomer(firstname, lastname, street, city, email, username, password);
	}
	
	public Credentials updatedCredentials() {
		if(oldPassword == null		|| oldPassword.length == 0)		return null;
		if(password == null			|| password.length == 0)		return null;
		if(confirmPassword == null	|| confirmPassword.length == 0)	return null;
		
		if(password.length != confirmPassword.length) return null;
		for (int i = 0; i < password.length; i++)
			if(password[i] != confirmPassword[i])
				 return null;
		
		return new Credentials(oldPassword, password);	
	}

	@Override
	public String toString() {
		return "CustomerData [firstname=" + firstname + ", lastname=" + lastname + ", street=" + street + ", city="
				+ city + ", email=" + email + ", username=" + username + ", password=" + Arrays.toString(password)
				+ ", confirmPassword=" + Arrays.toString(confirmPassword) + ", oldPassword="
				+ Arrays.toString(oldPassword) + "]";
	}
}
