package de.stuttgart.dhbw.bookapp.client.model;

public class CustomerSession {
	
	private static final int TIMEOUT = 10; // min
	
	private String username;
	private String sessionId;
	private Thread life;
	
	public CustomerSession() {
	}

	public boolean loggedIn() {
		return sessionId != null;
	}

	public void logOut() {
		if(life != null) life.interrupt();
	}

	public void setUsernameAndSessionId(String username, String sessionId) {
		this.logOut();
		this.username = username;
		this.sessionId = sessionId;
		
		life = new Thread(() -> {	try {
									Thread.sleep(TIMEOUT * 60 * 1000);
										} catch (InterruptedException e) { }
									this.username = null;
									this.sessionId = null;
		});
		life.start();
	}

	public String getUsername() {
		return username;
	}
	
	public String getSessionId() {
		return sessionId;
	}

	@Override
	public String toString() {
		return "CustomerSession [username=" + username + ", sessionId=" + sessionId + "]";
	}
}
